const express = require('express');
const generateControllers = require('../../util/query');
const Game = require('./model');

const gameController = generateControllers(Game);
const gameRouter = express.Router();

gameRouter
  .route('/')
  .get(gameController.getAll)
  .post(gameController.createOne);

gameRouter
  .route('/:id')
  .get(gameController.getOne)
  .put(gameController.updateOne)
  .delete(gameController.deleteOne);

module.exports = gameRouter;
