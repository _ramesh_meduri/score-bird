const express = require('express');
const generateControllers = require('../../util/query');
const Team = require('./model');

const teamController = generateControllers(Team);
const teamRouter = express.Router();

teamRouter
  .route('/')
  .get(teamController.getAll)
  .post(teamController.createOne);

teamRouter
  .route('/:id')
  .get(teamController.getOne)
  .put(teamController.updateOne)
  .delete(teamController.deleteOne);

module.exports = teamRouter;
