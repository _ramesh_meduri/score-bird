const AmazonCognitoIdentity = require('amazon-cognito-identity-js');
const jwt = require('jsonwebtoken');
const request = require('request');
const jwkToPem = require('jwk-to-pem');

const pool_region = 'us-east-1';
const poolData = {
  UserPoolId: 'us-east-1_lcOC1L8tJ',
  ClientId: '7ap8i1dh75p34a4suajicrf4or'
};
const userPool = new AmazonCognitoIdentity.CognitoUserPool(poolData);

function isValidToken(req, res, next) {
  let token = req.headers.authorization;
  if (!token) {
    return res.status(403).send({
      message: 'No Token Provided'
    });
  }

  request(
    {
      url: `https://cognito-idp.${pool_region}.amazonaws.com/${
        poolData.UserPoolId
      }/.well-known/jwks.json`,
      json: true
    },
    function(error, response, body) {
      if (!error && response.statusCode === 200) {
        pems = {};
        var keys = body['keys'];
        for (var i = 0; i < keys.length; i++) {
          //Convert each key to PEM
          var key_id = keys[i].kid;
          var modulus = keys[i].n;
          var exponent = keys[i].e;
          var key_type = keys[i].kty;
          var jwk = { kty: key_type, n: modulus, e: exponent };
          var pem = jwkToPem(jwk);
          pems[key_id] = pem;
        }
        //validate the token
        var decodedJwt = jwt.decode(token, { complete: true });
        if (!decodedJwt) {
          console.log('Not a valid JWT token');
          return res.status(403).send({
            message: 'Invalid Token'
          });
        }

        var kid = decodedJwt.header.kid;
        var pem = pems[kid];
        if (!pem) {
          console.log('Invalid token');
          return res.status(403).send({
            message: 'Invalid Token'
          });
        }

        jwt.verify(token, pem, function(err, payload) {
          if (err) {
            console.log('Invalid Token.');
            return res.status(403).send({
              message: 'Invalid Token'
            });
          } else {
            console.log('Valid Token.');
            console.log(payload);
            return next();
          }
        });
      } else {
        console.log('Error! Unable to download JWKs');
      }
    }
  );
}

module.exports = { isValidToken };
