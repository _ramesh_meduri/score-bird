const mongoose = require('mongoose');
const express = require('express');
const generateControllers = require('../../util/query');
const School = require('./model');
const User = require('../user/model');
const Facility = require('../facility/model');
const ObjectId = mongoose.Types.ObjectId;

const getSchoolsPerEmail = (req, res, next) => {
  const email = req.params.email;

  User.aggregate([
    { $match: { email } },
    {
      $lookup: {
        from: 'organisations',
        localField: '_id',
        foreignField: 'created_by',
        as: 'orgdata'
      }
    },
    { $unwind: '$orgdata' },
    {
      $lookup: {
        from: 'schools',
        localField: 'orgdata._id',
        foreignField: 'org_id',
        as: 'schools'
      }
    }
  ]).exec((err, docs) => {
    if (err) {
      return next(err);
    }
    res.json(docs[0].schools);
  });
};

const getSchoolData = (req, res, next) => {
  const school_id = req.params.id;
  Facility.aggregate([
    { $match: { school_id: ObjectId(school_id) } },
    {
      $lookup: {
        from: 'schools',
        localField: 'school_id',
        foreignField: '_id',
        as: 'school_data'
      }
    },
    {
      $lookup: {
        from: 'facility_types',
        localField: 'facility_type_id',
        foreignField: '_id',
        as: 'facility_type_data'
      }
    },
    {
      $lookup: {
        from: 'scoreboards',
        localField: 'scoreboard_id',
        foreignField: '_id',
        as: 'scoreboard_type_data'
      }
    },
    {
      $lookup: {
        from: 'venues',
        localField: 'venue_id',
        foreignField: '_id',
        as: 'venue_type_data'
      }
    }
  ]).exec((err, docs) => {
    if (err) {
      return next(err);
    }
    res.json(docs);
  });
};

const getStadiumData = (req, res, next) => {
  const school_id = req.params.id;

  Facility.aggregate([
    { $match: { _id: ObjectId(school_id) } },
    {
      $lookup: {
        from: 'schools',
        localField: 'school_id',
        foreignField: '_id',
        as: 'school_data'
      }
    },

    {
      $lookup: {
        from: 'facility_types',
        localField: 'facility_type_id',
        foreignField: '_id',
        as: 'facility_type_data'
      }
    },
    {
      $lookup: {
        from: 'scoreboards',
        localField: 'scoreboard_id',
        foreignField: '_id',
        as: 'scoreboard_type_data'
      }
    },
    {
      $lookup: {
        from: 'venues',
        localField: 'venue_id',
        foreignField: '_id',
        as: 'venue_type_data'
      }
    }
  ]).exec((err, docs) => {
    if (err) {
      return next(err);
    }
    res.json(docs);
  });
};


const schoolController = generateControllers(School, {
  getSchoolsPerEmail,
  getSchoolData,
  getStadiumData
});
const schoolRouter = express.Router();


schoolRouter.route('/').get(schoolController.getAll)
                       .post(schoolController.createOne);

schoolRouter.route('/:email').get(getSchoolsPerEmail);

schoolRouter.route('/schoolDetails/:id').get(getSchoolData);

schoolRouter.route('/stadiumDetails/:id').get(getStadiumData);

module.exports = schoolRouter;
