import { SHOW_CONTAINER_MENU, HIDE_CONTAINER_MENU } from './types';

const showContainerMenu = () => ({
  type: SHOW_CONTAINER_MENU
});

const hideContainerMenu = () => ({
  type: HIDE_CONTAINER_MENU
});

export { showContainerMenu, hideContainerMenu };
