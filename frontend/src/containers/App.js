import React, { Component } from 'react';
import { Redirect, Route } from 'react-router-dom';
import { connect } from 'react-redux';

import CustomerPanel from '../app/customerPanel';
import SignIn from './SignIn';
import { setInitUrl } from '../actions/authActions';

const RestrictedRoute = ({ component: Component, user, ...rest }) => (
  <Route
    {...rest}
    render={(props) =>
      user ? (
        <Component {...props} />
      ) : (
        <Redirect
          to={{
            pathname: '/signin',
            state: { from: props.location }
          }}
        />
      )
    }
  />
);

class App extends Component {
  componentDidMount() {
    if (this.props.initURL === '') {
      this.props.setInitUrl(this.props.history.location.pathname);
    }
  }

  render() {
    const { match, location, user, initURL } = this.props;

    console.log('-------App.js------');
    console.log(this.props);
    console.log('------App.js-------');

    if (location.pathname === '/') {
      if (user === null) {
        return <Redirect to={'/signin'} />;
      } else if (initURL === '' || initURL === '/' || initURL === '/signin') {
        return <Redirect to={'/app/schools'} />;
      } else {
        return <Redirect to={initURL} />;
      }
    }

    return (
      <div className="app-main">
        <RestrictedRoute
          path={`${match.url}app`}
          user={user}
          component={CustomerPanel}
        />
        <Route path="/signin" component={SignIn} />
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  user: state.auth.user,
  initURL: state.auth.initURL
});

export default connect(
  mapStateToProps,
  { setInitUrl }
)(App);
