const express = require('express');
const generateControllers = require('../../util/query');
const Facility = require('./model');

const facilityController = generateControllers(Facility);
const facilityRouter = express.Router();

facilityRouter
  .route('/')
  .get(facilityController.getAll)
  .post(facilityController.createOne);

facilityRouter
  .route('/:id')
  .get(facilityController.getOne)
  .put(facilityController.updateOne)
  .delete(facilityController.deleteOne);

module.exports = facilityRouter;
