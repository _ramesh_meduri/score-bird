const mongoose = require('mongoose');

const UserSchema = new mongoose.Schema(
  {
    date_created: { type: Date },
    date_modified: { type: Date, default: Date.now },
    email: { type: String },
    first_name: { type: String },
    last_name: { type: String },
    middle_name: String,
    phone: String,
    school: { type: String },
    school_id: { type: mongoose.Schema.Types.ObjectId, ref: 'School' }
  },
  { versionKey: false }
);

const User = mongoose.model('User', UserSchema);

module.exports = User;
