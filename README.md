### Scorebird(Customer Portal) -- MERN Application
------
##### Description
Here a person from a `School` or `Organisation` can view their school, facilities, teams and games information


##### Backend Tech Stack
> Node  
> Express  
> MongoDB  
> Mongoose


##### Frontend Tech Stack
> React  
> React Router  
> Redux  
> Redux Thunk  
> Atlaskit  


##### Commands for Local Development

Start the `MongoDB` server `mongodb://localhost:27017/score`
```sh
cd backend
npm install
npm start
```

Server Starts Listening on http://localhost:4000
```sh
cd frontend
npm install
npm start
```
Client Starts Listening on http://localhost:3000
