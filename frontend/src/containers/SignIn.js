import React, { Component } from 'react';
import { Form, FormGroup, InputGroup } from 'reactstrap';
import { connect } from 'react-redux';

import TextField from '@atlaskit/field-text';
import Button from '@atlaskit/button';
import { Checkbox } from '@atlaskit/checkbox';
import { Link } from 'react-router-dom';

import { loginUser } from '../actions/authActions';
import login_img from '../assets/images/login_img.png';
import './SignIn.css';

class SignIn extends Component {
  state = {
    email: '',
    password: ''
  };

  componentDidUpdate(prevProps, prevState) {
    let nextProps = this.props;
    if (nextProps.user) {
      nextProps.history.push('/app/schools');
    }
  }

  onChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };

  componentDidMount() {
    window.addEventListener('keydown', this.onKeyDown);
  }

  componentWillUnmount() {
    window.removeEventListener('keydown', this.onKeyDown);
  }

  onKeyDown(e) {
    if (e.keyCode !== 13) return;
    this.onSubmit();
  }

  resendLink = () => {
    this.setState({ error: null });
  };

  onSubmit = (e) => {
    console.log('onSubmit = (e) => {');
    e.preventDefault();
    const userData = {
      email: this.state.email,
      password: this.state.password
    };
    this.props.loginUser(userData);
  };
  render() {
    const { email, password } = this.state;
    return (
      <div className="signin-container">
        <style />
        <div className="signup-block">
          <div className="signup-container">
            <img src={login_img} alt="" />
            <h2>Let's Get Started!</h2>
            <p>
              Sign up to manage facilities, schools,
              <br /> teams and more...
            </p>
            <Link to="/sign_up" style={{ textDecoration: 'none' }}>
              <Button variant="primary" className="rounded-btn" active>
                Sign Up
              </Button>
            </Link>
          </div>
        </div>
        <div className="signin-block">
          <div className="signin-form">
            <Form onSubmit={this.onSubmit}>
              <h1>Welcome To Scorebird</h1>
              <p className="text-muted">
                Sign in by entering the information below
              </p>

              {this.state.success ? (
                <div className="alert alert-success mb-3">
                  {this.state.success}
                </div>
              ) : null}

              {this.state.error ? (
                <div className="alert alert-danger mb-3">
                  {this.state.error.message}

                  {this.state.allowReconfirm ? (
                    <div>
                      <br />
                      <Button
                        color="link"
                        className="px-0"
                        onClick={this.resendLink}
                      >
                        Click here to resend the code.
                      </Button>
                    </div>
                  ) : null}
                </div>
              ) : null}

              <InputGroup className="mb-3">
                <TextField
                  label="Email"
                  autoFocus
                  placeholder="mail@user.com"
                  autoComplete="email"
                  key="email"
                  name="email"
                  required={true}
                  onChange={this.onChange}
                  value={email}
                />
                <div className="alert alert-danger mb-3">
                  {this.state.errormessage}
                </div>
              </InputGroup>

              <InputGroup className="mb-4">
                <TextField
                  label="Password"
                  type="password"
                  placeholder="Password"
                  key="password"
                  name="password"
                  required={true}
                  autoComplete="current-password"
                  onChange={this.onChange}
                  value={password}
                />
                <div className="alert alert-danger mb-3">
                  {this.state.passwordemptyerror}
                </div>
              </InputGroup>

              <div className="actions-row">
                <FormGroup check className="mb-3">
                  <Checkbox
                    value="Remember Me"
                    label="Remember Me"
                    onChange={this.onChange}
                    name="checkbox-basic"
                  />
                </FormGroup>
                <label
                  className="forget-link"
                  onClick={() => this.props.history.push('/forgot-password')}
                >
                  Forgotten password
                </label>
              </div>

              <Button
                className={
                  'btn brand-btn signin-btn ' +
                  (this.state.loading && 'loading')
                }
                appearance="primary"
                onClick={this.onSubmit}
                isDisabled={this.state.loading}
              >
                Sign In
              </Button>
            </Form>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  user: state.auth.user
});

export default connect(
  mapStateToProps,
  { loginUser }
)(SignIn);
