const mongoose = require('mongoose');
const Facility = require('../../resources/facility/model');
const data = require('./facility_scoreboard_map.json');

function main() {
  mongoose.Promise = global.Promise;
  mongoose
    .connect('mongodb://localhost:27017/scorebird_customer', {
      useNewUrlParser: true
    })
    .then(() => {
      console.log('MongoDB Connected !');
      start();
    })
    .catch((err) => console.log(err));
}

const start = async () => {
  let len = data.length;
  console.log(len);
  for (let i = 0; i < len; i++) {
    let obj = data[i];
    let facilityObj = await Facility.findOne({ _id: obj.facility_id });
    facilityObj.scoreboard_id = obj.scoreboard_id;
    delete facilityObj._id;
    console.log(facilityObj);

    let updatedFacilityObj = await Facility.findByIdAndUpdate(
      obj.facility_id,
      facilityObj,
      { new: true }
    );
    console.log(updatedFacilityObj);
  }
  console.log('finished !');
};

main();
