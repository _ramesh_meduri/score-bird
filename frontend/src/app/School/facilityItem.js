import React, { Fragment, Component } from 'react';
import { Link } from 'react-router-dom';
import Button from '@atlaskit/button';
import Form, { Field, Fieldset, CheckboxField } from '@atlaskit/form';
import Select from '@atlaskit/select';
import { Checkbox } from '@atlaskit/checkbox';
import ArrowLeftIcon from '@atlaskit/icon/glyph/arrow-left';
import TextField from '@atlaskit/field-text';
import { connect } from 'react-redux';
import { FieldTextAreaStateless } from '@atlaskit/field-text-area';
import { getStadiumDetails } from '../../actions/schoolActions';

const facilitytype = [
  { label: 'School1 Stadium', value: 'School1 Stadium' },
  { label: 'School2 Stadium', value: 'School2 Stadium' }
];
const state = [
  { label: 'Texas', value: 'Texas' },
  { label: 'Florida', value: 'Florida' }
];
const city = [
  { label: 'New York city', value: 'New York City ' },
  { label: 'Los Angeles', value: 'Los Angeles' }
];
const schoolName = [
  { label: 'Belton High school', value: 'Belton High school' },
  { label: 'Ellison High school', value: 'Ellison High School' }
];
const isfacility = [
  { label: 'No', value: 'no' },
  { label: 'Yes', value: 'yes' }
];
const Scoreboard = [
  { label: 'Digital', value: 'Digital' },
  { label: 'Analog', value: 'Analog' }
];

const Header = (props) => (
  <div className="content-header">
    <div className="content-header-inner header-row">
      <div className="left-panel back-with-heading">
        <div
          className="back-to-text"
          onClick={!props.isEdit ? props.goBack : props.editFacilityForm}
        >
          <span className="back-icon" to="/">
            <ArrowLeftIcon />
          </span>
          <b>
            <a className="back-to-text anchor_no_line" onClick={props.goBack}>
              Back to School Facilities
            </a>
          </b>
        </div>
      </div>
      <div className="right-panel">
        <div>
          {!props.isEdit ? (
            <Button
              appearance="primary"
              className="custom-btn"
              onClick={props.editFacilityForm}
            >
              Edit Facility{' '}
            </Button>
          ) : null}
        </div>
      </div>
    </div>
  </div>
);

class Stadiumdetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isEdit: false,
      schoolsDetails: null,
      loading: true,
      schoolform: null,
      ethernetChecked: false,
      wifiChecked: false,
      is_facility: 'no'
    };
  }

  goBack = () => {
    let schoolId = this.props.location.state.data[0].school_id;
    this.props.history.push(`/app/facilities/${schoolId}`);
  };

  editFacilityForm = () => {
    this.setState({
      isEdit: !this.state.isEdit
    });
  };
  componentDidMount() {
    let facilityId = this.props.match.params.facilityId;
    if (facilityId) {
      this.props.getStadiumDetails(facilityId);
    }
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    let obj = nextProps.stadiumDetails[0];
    if (obj) {
      return {
        ...prevState,
        schoolsDetails: obj,
        schoolform: obj,
        loading: false,
        ethernetChecked: obj.ethernet_access !== 'No' ? true : false,
        wifiChecked: obj.Wifi_access !== 'No' ? true : false,
        is_facility: obj.is_facility || 'yes'
      };
    }
    return null;
  }

  changeState = (e, value) => {
    console.log(e);
    if (value === 'ethernet') {
      this.setState({
        ethernetChecked: !this.state.ethernetChecked
      });
    }
    if (value === 'wifi') {
      this.setState({
        wifiChecked: !this.state.wifiChecked
      });
    }
    if (value === 'isfacility') {
      this.setState({
        is_facility: e.value
      });
      console.log(this.state.is_facility);
    }
  };
  backTofacilityDetail = () => {
    this.setState({
      isEdit: !this.state.isEdit
    });
  };
  renderForm() {
    var normalView = {
      display: this.state.isEdit ? 'none' : 'flex'
    };

    var editView = {
      display: this.state.isEdit ? 'flex' : 'none'
    };

    return (
      <div>
        {this.state.loading ? null : (
          <div className="school-stadium-info">
            <div />

            <div className="view-info" style={normalView}>
              <div className="non-edit-field">
                <label>Facility Name</label>
                <div className="field-value">
                  {this.state.schoolsDetails.name}
                </div>
              </div>
              <div className="non-edit-field">
                <label>School Name</label>
                <div className="field-value">
                  {this.state.schoolsDetails.school_data.length
                    ? this.state.schoolsDetails.school_data[0].name
                    : '--'}
                </div>
              </div>
              <div className="non-edit-field">
                <label>Facility Type</label>
                <div className="field-value">
                  {this.state.schoolsDetails.facility_type_data.length
                    ? this.state.schoolsDetails.facility_type_data[0].name
                    : '--'}
                </div>
              </div>
              <div className="non-edit-field" />
              <div className="non-edit-field">
                <label>Is Facility at School</label>
                <div className="field-value">
                  {this.state.schoolsDetails.is_facility
                    ? this.state.schoolsDetails.is_facility
                    : '--'}
                </div>
              </div>
              <div className="non-edit-field" />
              <div className="non-edit-field" />
              <div className="non-edit-field">
                <label>Venue</label>
                <div className="field-value">
                  {this.state.schoolsDetails.venue_type_data.length
                    ? this.state.schoolsDetails.venue_type_data[0].name
                    : '--'}
                </div>
              </div>
              <div className="non-edit-field">
                <label>Venue City</label>
                <div className="field-value">
                  {this.state.schoolsDetails.venue_type_data.length
                    ? this.state.schoolsDetails.venue_type_data[0].venue_city
                    : '--'}
                </div>
              </div>
              <div className="non-edit-field">
                <label>Venue Address</label>
                <div className="field-value">--</div>
              </div>
              <div className="non-edit-field">
                <label>Sports Played at Venue</label>
                <div className="field-value">--</div>
              </div>
              <div className="non-edit-field" />
              <div className="non-edit-field" />
              <div className="non-edit-field">
                <label>Scorebird Type</label>
                <div className="field-value">
                  {this.state.schoolsDetails.scoreboard_type_data.length
                    ? this.state.schoolsDetails.scoreboard_type_data[0].name
                    : '--'}
                </div>
              </div>
              <div className="non-edit-field">
                <label>Internet Available Via</label>
                <div className="field-value">
                  {this.state.schoolsDetails.ethernet_access == 'Yes'
                    ? 'Yes'
                    : 'No'}
                  {this.state.schoolsDetails.wifi_access !== 'No' ? (
                    <p>WiFi</p>
                  ) : null}
                </div>
              </div>
            </div>
          </div>
        )}
        {this.state.schoolform ? (
          <div className="edit-info" style={editView}>
            <div className="edit-facility-form">
              <Form
                onSubmit={(data) => {
                  data.unique_id = this.state.schoolform._id;
                  data.Ethernet = this.state.ethernetChecked;
                  data.wifi = this.state.wifiChecked;
                  this.sbs.saveFacilityDetails(data);
                }}
              >
                {({ formProps }) => (
                  <form {...formProps}>
                    <Field
                      name="facilitytype"
                      label="Facility Type"
                      defaultValue={
                        this.state.schoolform.facility_type_data.length
                          ? this.state.schoolform.facility_type_data[0].name
                          : ''
                      }
                      value={{
                        label: this.state.schoolform.facility_type_data.length
                          ? this.state.schoolform.facility_type_data[0].name
                          : '',
                        value: this.state.schoolform.facility_type_data.length
                          ? this.state.schoolform.facility_type_data[0].name
                          : ''
                      }}
                      className="facility-spec"
                    >
                      {({ fieldProps }) => (
                        <TextField
                          placeholder="enter facility type"
                          {...fieldProps}
                        />
                      )}
                    </Field>
                    <Field
                      name="name"
                      label="Facility Name"
                      className="facility-spec"
                      defaultValue={this.state.schoolform.name}
                      value={this.state.schoolform.name}
                    >
                      {({ fieldProps }) => (
                        <TextField
                          placeholder="enter facility name"
                          {...fieldProps}
                        />
                      )}
                    </Field>
                    <Field
                      name="isfacility"
                      label="Is Facility at School"
                      defaultValue={{
                        label: this.state.is_facility,
                        value: this.state.value
                      }}
                      value={{
                        label: this.state.is_facility,
                        value: this.state.value
                      }}
                      className="facility-spec"
                    >
                      {({ fieldProps: { id, ...rest } }) => (
                        <Fragment>
                          <Select
                            inputId={id}
                            {...rest}
                            options={isfacility}
                            onChange={(e) => this.changeState(e, 'isfacility')}
                          />
                        </Fragment>
                      )}
                    </Field>

                    {this.state.is_facility === 'no' ? (
                      <div>
                        <Field
                          name="venue_name"
                          defaultValue={
                            this.state.schoolform.venue_type_data.length
                              ? this.state.schoolform.venue_type_data[0].name
                              : ''
                          }
                          value={
                            this.state.schoolform.venue_type_data.length
                              ? this.state.schoolform.venue_type_data[0].name
                              : ''
                          }
                          label="Venue Name"
                          className="facility-spec"
                        >
                          {({ fieldProps }) => (
                            <TextField
                              placeholder="enter venue name"
                              {...fieldProps}
                            />
                          )}
                        </Field>
                        <Field
                          name="venue_city"
                          label="Venue City"
                          defaultValue={[]}
                          className="facility-spec"
                        >
                          {({ fieldProps: { id, ...rest } }) => (
                            <Fragment>
                              <Select
                                inputId={id}
                                placeholder="select venue city"
                                {...rest}
                                options={city}
                              />
                            </Fragment>
                          )}
                        </Field>
                        <Field
                          name="Venue_address"
                          defaultValue=""
                          label="Venue Address"
                          className="facility-spec"
                        >
                          {({ fieldProps }) => (
                            <TextField
                              placeholder="enter venue address"
                              {...fieldProps}
                            />
                          )}
                        </Field>
                      </div>
                    ) : null}
                    <div className="fieldset-block">
                      <Fieldset
                        legend="Sport Played at the Facility"
                        className="facility-spec"
                      >
                        <CheckboxField name="sport" value="Football">
                          {({ fieldProps }) => (
                            <Checkbox {...fieldProps} label="Football" />
                          )}
                        </CheckboxField>
                        <CheckboxField name="sport" value="Boys Soccer">
                          {({ fieldProps }) => (
                            <Checkbox {...fieldProps} label="Boys Soccer" />
                          )}
                        </CheckboxField>
                        <CheckboxField name="sport" value="Girls Soccer">
                          {({ fieldProps }) => (
                            <Checkbox {...fieldProps} label="Girls Soccer" />
                          )}
                        </CheckboxField>
                      </Fieldset>
                    </div>
                    <Field
                      name="scoreboard"
                      label="Scoreboard Type"
                      defaultValue={
                        this.state.schoolform.scoreboard_type_data.length
                          ? this.state.schoolform.scoreboard_type_data[0].name
                          : ''
                      }
                      value={{
                        label: this.state.schoolform.scoreboard_type_data.length
                          ? this.state.schoolform.scoreboard_type_data[0].name
                          : '',
                        value: this.state.schoolform.scoreboard_type_data.length
                          ? this.state.schoolform.scoreboard_type_data[0].name
                          : ''
                      }}
                      className="facility-spec"
                    >
                      {({ fieldProps: { id, ...rest } }) => (
                        <Fragment>
                          <Select
                            inputId={id}
                            placeholder="select scoreboared type"
                            {...rest}
                            options={Scoreboard}
                          />
                        </Fragment>
                      )}
                    </Field>
                    <div className="fieldset-block">
                      <Fieldset
                        legend="Internet available via"
                        className="facility-spec"
                      >
                        <CheckboxField
                          name="Ethernet"
                          isChecked={this.state.ethernetChecked}
                        >
                          {({ fieldProps }) => (
                            <Checkbox
                              {...fieldProps}
                              label="Ethernet"
                              isChecked={this.state.ethernetChecked}
                              onChange={(e) => this.changeState(e, 'ethernet')}
                            />
                          )}
                        </CheckboxField>
                        <CheckboxField
                          name="wifi"
                          isChecked={this.state.wifiChecked}
                        >
                          {({ fieldProps }) => (
                            <Checkbox
                              {...fieldProps}
                              label="wifi"
                              isChecked={this.state.wifiChecked}
                              onChange={(e) => this.changeState(e, 'wifi')}
                            />
                          )}
                        </CheckboxField>
                      </Fieldset>
                    </div>
                    <Field
                      name="comments"
                      defaultValue=""
                      label="Additional comments"
                      className="facility-spec"
                    >
                      {({
                        fieldProps: { isRequired, isDisabled, ...others }
                      }) => (
                        <FieldTextAreaStateless
                          isLabelHidden
                          shouldFitContainer
                          placeholder="enter additional info"
                          disabled={isDisabled}
                          required={isRequired}
                          {...others}
                        />
                      )}
                    </Field>

                    <div className="footer-actions">
                      <div />
                      <div className="actions">
                        <Button
                          type="submit"
                          appearance="default"
                          onClick={this.backTofacilityDetail}
                        >
                          Cancel
                        </Button>
                        <Button type="submit" appearance="primary">
                          Save
                        </Button>
                      </div>
                    </div>
                  </form>
                )}
              </Form>
            </div>
          </div>
        ) : null}
      </div>
    );
  }
  renderLoader() {}
  render() {
    return (
      <div className="full-width-container">
        <Header
          editFacilityForm={this.editFacilityForm}
          goBack={this.goBack}
          isEdit={this.state.isEdit}
        />
        {this.renderForm()}
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  stadiumDetails: state.appData.stadiumDetails
});

export default connect(
  mapStateToProps,
  { getStadiumDetails }
)(Stadiumdetails);
