import {
  SCHOOL_LIST,
  SELECTED_SCHOOL,
  FACILITY_LIST,
  STADIUM_DETAIL,
  FACILITY_TYPE_LIST,
  SCOREBOARD_TYPES_LIST,
  CREATE_FACILITY
} from '../actions/types';

const INIT_STATE = {
  schoolList: [],
  selectedSchool: {},
  facilityList: [],
  scoreboardTypeList: [],
  facilityTypeList: [],
  stadiumDetails: [],
  facility: {}
};

export default (state = INIT_STATE, action) => {
  switch (action.type) {
    case SCHOOL_LIST: {
      return {
        ...state,
        schoolList: action.payload
      };
    }

    case SELECTED_SCHOOL: {
      return {
        ...state,
        selectedSchool: action.payload
      };
    }

    case FACILITY_LIST: {
      return {
        ...state,
        facilityList: action.payload
      };
    }

    case FACILITY_TYPE_LIST: {
      return {
        ...state,
        facilityTypes: action.payload
      };
    }

    case SCOREBOARD_TYPES_LIST: {
      return {
        ...state,
        scoreboardTypes: action.payload
      };
    }

    case STADIUM_DETAIL: {
      return {
        ...state,
        stadiumDetails: action.payload
      };
    }

    case CREATE_FACILITY: {
      return {
        ...state,
        facility: action.payload
      };
    }

    default:
      return state;
  }
};
