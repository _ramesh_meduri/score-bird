const express = require('express');
const generateControllers = require('../../util/query');
const State = require('./model');

const stateController = generateControllers(State);
const stateRouter = express.Router();

stateRouter
  .route('/')
  .get(stateController.getAll)
  .post(stateController.createOne);

stateRouter
  .route('/:id')
  .get(stateController.getOne)
  .put(stateController.updateOne)
  .delete(stateController.deleteOne);

module.exports = stateRouter;
