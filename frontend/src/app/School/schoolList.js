import React, { Component } from 'react';
import { connect } from 'react-redux';
import { AgGridReact } from 'ag-grid-react';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-material.css';
import { Redirect } from 'react-router-dom';
import Button from '@atlaskit/button';
import SearchIcon from '@atlaskit/icon/glyph/search';
import TextField from '@atlaskit/field-text';
import Spinner from '@atlaskit/spinner';

import AddSchoolForm from './addSchoolForm';
import { getSchoolList, setSelectedSchool } from '../../actions/schoolActions';

const Header = (props) => (
  <div className="content-header">
    <div className="content-header-inner header-row">
      <div className="left-panel">
        <h1 className="content-label"> Schools</h1>
      </div>
      <div className="right-panel">
        <div>
          <Button
            appearance="default"
            className="custom-btn"
            onClick={props.toggleAddSchool}
          >
            Add School
          </Button>
        </div>
        <div className="content-search">
          <div className="search-icon">
            <SearchIcon />
            <TextField
              type="text"
              placeholder="Search"
              label="hidden label"
              isLabelHidden
              value={props.value}
              onChange={props.channge}
            />
          </div>
        </div>
      </div>
    </div>
  </div>
);

class School extends Component {
  constructor(props) {
    super(props);
    this.state = {
      updatePhase: false,
      value: '',
      shouldDisplayContainerNav: true,
      redirect: false,
      addSchool: false,
      columnDefs: [
        { headerName: 'SCHOOL NAME', field: 'name' },
        {
          headerName: 'CITY & STATE',
          cellRenderer: function(params) {
            return (
              params.data.city +
              (params.data.city ? ',' : '') +
              params.data.state
            );
          }
        },
        {
          headerName: 'POSTAL CODE ',
          cellRenderer: function(params) {
            return params.data.postal_Code ? params.data.postal_Code : '-  -';
          }
        },
        { headerName: 'MODIFIED_DATE', field: 'date_modified' }
      ],

      DefaultcolDef: {
        width: 100,
        editable: true,
        isEdit: false,
        lockPosition: true,
        filter: 'agTextColumnFilter'
      },
      allschools: null,
      rowData: null,
      initialList: null,
      loading: true
    };
  }

  componentDidMount() {
    let email = this.props.email;
    if (email) {
      this.props.getSchoolList(email);
    }
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    let arr = nextProps.schoolList;
    if (arr.length && !prevState.updatePhase) {
      return {
        ...prevState,
        loading: false,
        rowData: arr,
        initialList: arr
      };
    }
    return null;
  }

  filterNames = (event) => {
    if (
      (this.updatedList && this.updatedList.length) ||
      (this.updatedList && this.updatedList.length === 0)
    ) {
      this.updatedList = this.state.initialList;
    } else {
      this.updatedList = this.state.rowData;
    }

    this.updatedList = this.updatedList.filter(function(item) {
      if (item.name && item.city && item.state) {
        return (
          item.name.toLowerCase().search(event.target.value.toLowerCase()) !==
            -1 ||
          item.city.toLowerCase().search(event.target.value.toLowerCase()) !==
            -1 ||
          item.state.toLowerCase().search(event.target.value.toLowerCase()) !==
            -1
        );
      }
    });

    if (event.target.value.length) {
      let arr = this.updatedList.concat();
      this.setState(
        {
          rowData: arr,
          updatePhase: true,
          value: event.target.value
        },
        () => {
          console.log('rowData updated');
        }
      );
    } else {
      this.setState({
        rowData: this.state.initialList,
        value: event.target.value
      });
    }
  };

  toggleAddSchool = () => {
    this.setState({
      addSchool: !this.state.addSchool
    });
  };

  toggleContainerNav = () => {
    this.setState((state) => ({
      shouldDisplayContainerNav: false
    }));
  };

  getGridData = () => {
    if (this.state.loading) {
      return (
        <div className="loader">
          <Spinner size="large" />
        </div>
      );
    } else {
      var noRowsTemplate = '<span>No devices to show.</span>';
      return (
        <div style={{ display: 'flex', flexDirection: 'row' }}>
          <div style={{ overflow: 'hidden', flexGrow: '1' }}>
            <div
              className="ag-theme-material ag-custom-styles device-grid"
              style={{
                height: 'calc(100vh - 60px)',
                width: '100%'
              }}
            >
              <AgGridReact
                columnDefs={this.state.columnDefs}
                defaultColDef={this.state.defaultColDef}
                rowSelection={this.state.rowSelection}
                enableColResize={true}
                enableSorting={true}
                enableFilter={false}
                rowHeight="48"
                headerHeight="36"
                suppressDragLeaveHidesColumns={true}
                suppressRowClickSelection={true}
                rowData={this.state.rowData}
                getRowNodeId={this.state.getRowNodeonline}
                onGridReady={this.state.onGridReady}
                onGridReady={this.onGridReady.bind(this)}
                onRowClicked={this.onRowClicked}
                onSelectionChanged={this.onSelectionChanged}
                pagination={true}
                paginationPageSize="20"
                overlayNoRowsTemplate={noRowsTemplate}
              />
            </div>
          </div>
        </div>
      );
    }
  };

  setRowRedirect = (schoolId) => {
    this.setState({
      selectedSchool: schoolId,
      redirect: true
    });
  };

  onRowClicked = (event) => {
    let schoolId = event.data._id;
    this.setRowRedirect(schoolId);
    this.props.setSelectedSchool(event.data);
  };

  renderRowRedirect = () => {
    if (this.state.redirect) {
      let schoolId = this.state.selectedSchool;
      return <Redirect to={'/app/facilities/' + schoolId} />;
    }
  };

  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    params.api.sizeColumnsToFit();
    window.addEventListener('resize', function() {
      setTimeout(function() {
        params.api.sizeColumnsToFit();
      });
    });
    params.api.sizeColumnsToFit();
  }

  render() {
    return (
      <div
        style={{
          width: 'calc(100% - 64px)',
          left: '64px',
          position: 'absolute'
        }}
      >
        {this.state.addSchool ? (
          <AddSchoolForm cancel={this.toggleAddSchool} />
        ) : (
          <div>
            <div>
              <Header
                toggleAddSchool={this.toggleAddSchool}
                value={this.state.value}
                channge={(event) => this.filterNames(event)}
              />
            </div>
            <div style={{ height: '100%' }}>
              {this.getGridData()}
              {this.renderRowRedirect()}
            </div>
          </div>
        )}
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  schoolList: state.appData.schoolList,
  email: state.auth.user.email
});

export default connect(
  mapStateToProps,
  { getSchoolList, setSelectedSchool }
)(School);
