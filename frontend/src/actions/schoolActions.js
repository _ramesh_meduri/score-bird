import axios from 'axios';
import {
  SCHOOL_LIST,
  SELECTED_SCHOOL,
  FACILITY_LIST,
  STADIUM_DETAIL,
  FACILITY_TYPE_LIST,
  SCOREBOARD_TYPES_LIST,
  CREATE_FACILITY,
  CREATE_SCHOOL
} from './types';

// get schools
const getSchoolList = (email) => (dispatch) => {
  axios
    .get(`/api/school/${email}`)
    .then((res) => {
      dispatch({ type: SCHOOL_LIST, payload: res.data });
    })
    .catch((err) => {
      console.log(err);
    });
};

// get facilites
const getFacilityList = (schoolId) => (dispatch) => {
  axios
    .get(`/api/school/schoolDetails/${schoolId}`)
    .then((res) => {
      dispatch({ type: FACILITY_LIST, payload: res.data });
    })
    .catch((err) => {
      console.log(err);
    });
};

const getFacilityTypes = () => (dispatch) => {
  console.log('const getFacilityTypes = () => (dispatch) => {');
  axios
    .get(`/api/facilityType`)
    .then((res) => {
      dispatch({ type: FACILITY_TYPE_LIST, payload: res.data });
    })
    .catch((err) => {
      console.log(err);
    });
};

const getScoreboardTypes = () => (dispatch) => {
  axios
    .get(`/api/scoreboard`)
    .then((res) => {
      dispatch({ type: SCOREBOARD_TYPES_LIST, payload: res.data });
    })
    .catch((err) => {
      console.log(err);
    });
};

const getStadiumDetails = (facilityId) => (dispatch) => {
  axios
    .get(`/api/school/stadiumDetails/${facilityId}`)
    .then((res) => {
      dispatch({ type: STADIUM_DETAIL, payload: res.data });
    })
    .catch((err) => {
      console.log(err);
    });
};

const postFacility = (payload) => (dispatch) => {
  axios
    .post(`/api/facility/`, payload)
    .then((res) => {
      dispatch({ type: CREATE_FACILITY, payload: res.data });
    })
    .catch((err) => {
      console.log(err);
    });
};
const postSchool= (payload) => (dispatch) => {
  axios
    .post(`/api/school/`, payload)
    .then((res) => {
      dispatch({ type: CREATE_SCHOOL, payload: res.data });
    })
    .catch((err) => {
      console.log(err);
    });
};

const setSelectedSchool = (schoolObj) => (dispatch) => {
  dispatch({ type: SELECTED_SCHOOL, payload: schoolObj });
};

export {
  getSchoolList,
  setSelectedSchool,
  getFacilityList,
  getStadiumDetails,
  getFacilityTypes,
  getScoreboardTypes,
  postFacility,
  postSchool
};
