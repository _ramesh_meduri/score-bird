const express = require('express');
const generateControllers = require('../../util/query');
const Schedule = require('./model');

const scheduleController = generateControllers(Schedule);
const scheduleRouter = express.Router();

scheduleRouter
  .route('/')
  .get(scheduleController.getAll)
  .post(scheduleController.createOne);

scheduleRouter
  .route('/:id')
  .get(scheduleController.getOne)
  .put(scheduleController.updateOne)
  .delete(scheduleController.deleteOne);

module.exports = scheduleRouter;
