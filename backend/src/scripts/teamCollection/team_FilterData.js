//teams.school_id=school._id

const path = require('path');
const fs = require('fs');
const mongoose = require('mongoose');
const Team = require('../../resources/team/model');
const School = require('../../resources/school/model');

function main() {
  mongoose.Promise = global.Promise;
  mongoose
    .connect('mongodb://localhost:27017/scorebird_customer', {
      useNewUrlParser: true
    })
    .then(() => {
      console.log('MongoDB Connected !');
      start();
    })
    .catch((err) => console.log(err));
}

function start() {
  Team.find()
    .then(cb)
    .catch((err) => {
      console.log(err);
    });
}

let cb = async (docs) => {
  let toBeProcessedArr = [];
  let TeamArr = docs.map((d) => d.toObject());
  let len = TeamArr.length;
  TeamArr.forEach((item) => {
    if (item.school_id) {
      toBeProcessedArr.push({
        team_id: item._id,
        school_id: item.school_id
      });
    }
  });
  let i, obj, schoolDoc;
  len = toBeProcessedArr.length;
  for (i = 0; i < len; i++) {
    obj = toBeProcessedArr[i];
    schoolDoc = await School.findOne({ unique_id: obj.school_id });
    if (schoolDoc) {
      obj.school_id = schoolDoc._id;
    }
  }
  let filePath = path.join(__dirname, 'team_school_map.json');
  console.log(filePath);
  fs.writeFile(filePath, JSON.stringify(toBeProcessedArr, null, 2), (err) => {
    if (err) {
      return console.log(err);
    }
    console.log('The file was saved!');
  });
};

main();
