// facilities.facility_type_id = facility_types._id

const path = require('path');
const fs = require('fs');
const mongoose = require('mongoose');
const Facility = require('../../resources/facility/model');
const FacilityType = require('../../resources/facility_type/model');

function main() {
  mongoose.Promise = global.Promise;
  mongoose
    .connect('mongodb://localhost:27017/scorebird_customer', {
      useNewUrlParser: true
    })
    .then(() => {
      console.log('MongoDB Connected !');
      start();
    })
    .catch((err) => console.log(err));
}

function start() {
  Facility.find()
    .then(cb)
    .catch((err) => {
      console.log(err);
    });
}

let cb = async (docs) => {
  let toBeProcessedArr = [];
  let facilityArr = docs.map((d) => d.toObject());
  let len = facilityArr.length;
  facilityArr.forEach((item) => {
    if (item.facility_type_id) {
      toBeProcessedArr.push({
        facility_id: item._id,
        facility_type_id: item.facility_type_id
      });
    }
  });

  let i, obj, facilityTypeDoc;
  len = toBeProcessedArr.length;
  for (i = 0; i < len; i++) {
    obj = toBeProcessedArr[i];
    facilityTypeDoc = await FacilityType.findOne({
      facility_type_id: obj.facility_type_id
    });
    if (facilityTypeDoc) {
      obj.facility_type_id = facilityTypeDoc._id;
    }
  }
  let filePath = path.join(__dirname, 'facility_facility_type_map.json');
  console.log(filePath);
  fs.writeFile(filePath, JSON.stringify(toBeProcessedArr, null, 2), (err) => {
    if (err) {
      return console.log(err);
    }
    console.log('The file was saved!');
  });
};

main();
