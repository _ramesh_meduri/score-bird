// facilities.venue_id = venue._id

const path = require('path');
const fs = require('fs');
const mongoose = require('mongoose');
const Facility = require('../../resources/facility/model');
const Venue = require('../../resources/venue/model');

function main() {
  mongoose.Promise = global.Promise;
  mongoose
    .connect('mongodb://localhost:27017/scorebird_customer', {
      useNewUrlParser: true
    })
    .then(() => {
      console.log('MongoDB Connected !');
      start();
    })
    .catch((err) => console.log(err));
}

function start() {
  Facility.find()
    .then(cb)
    .catch((err) => {
      console.log(err);
    });
}

let cb = async (docs) => {
  let toBeProcessedArr = [];
  let facilityArr = docs.map((d) => d.toObject());
  let len = facilityArr.length;
  facilityArr.forEach((item) => {
    if (item.venue_id) {
      toBeProcessedArr.push({
        facility_id: item._id,
        venue_id: item.venue_id
      });
    }
  });

  let i, obj, venueDoc;
  len = toBeProcessedArr.length;
  for (i = 0; i < len; i++) {
    obj = toBeProcessedArr[i];
    venueDoc = await Venue.findOne({ unique_id: obj.venue_id });
    if (venueDoc) {
      obj.venue_id = venueDoc._id;
    }
  }
  let filePath = path.join(__dirname, 'facility_venue_map.json');
  console.log(filePath);
  fs.writeFile(filePath, JSON.stringify(toBeProcessedArr, null, 2), (err) => {
    if (err) {
      return console.log(err);
    }
    console.log('The file was saved!');
  });
};

main();
