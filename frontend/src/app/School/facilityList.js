import React, { Component } from 'react';
import 'ag-grid-community/dist/styles/ag-grid.css';
import { AgGridReact } from 'ag-grid-react';

import Button from '@atlaskit/button';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import SchoolStadiumLeftNav from './schoolstadium-leftnav';
import Spinner from '@atlaskit/spinner';
import { Route } from 'react-router-dom';
import { getFacilityList } from '../../actions/schoolActions';
import AddSchoolForm from './addSchoolForm';

const teams = <h1>Teams</h1>;
const users = <h1>Users</h1>;

const Header = (props) => (
  <div className="content-header">
    <div className="content-header-inner header-row">
      <div className="left-panel">
        <div className="back-to-text">
          {props.schoolName}
          <p>/ Facilities</p>
        </div>
      </div>
      <div className="right-panel">
        <Button
          appearance="default"
          className="custom-btn"
          onClick={props.schooladdFacility}
        >
          Add Facility{' '}
        </Button>
      </div>
    </div>
  </div>
);

class FacilityList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      redirect: false,
      isEdit: false,
      tab: 'facilities',
      school_id: '',
      columnDefs: [
        {
          headerName: 'FACILITY NAME',
          cellRenderer: function(params) {
            return params.data.school_data.length ? params.data.name : ' - - ';
          }
        },
        {
          headerName: 'TYPE',
          cellRenderer: function(params) {
            return params.data.facility_type_data.length
              ? params.data.facility_type_data[0].name
              : ' - - ';
          }
        },
        {
          headerName: 'SCOREBOARD TYPE',
          cellRenderer: function(params) {
            return params.data.scoreboard_type_data.length
              ? params.data.scoreboard_type_data[0].name
              : ' - - ';
          }
        },
        { headerName: 'NO. OF SPORTS', field: 'noofsports' },
        { headerName: 'STATUS', field: 'status' }
      ],
      DefaultcolDef: {
        width: 100,
        editable: true,
        lockPosition: true,
        filter: 'agTextColumnFilter'
      },
      rowData: null,
      loading: true
    };
  }

  componentDidMount() {
    let schoolId = this.props.match.params.schoolId;
    if (schoolId) {
      this.props.getFacilityList(schoolId);
    }
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    let arr = nextProps.facilityList;
    if (arr.length) {
      return {
        ...prevState,
        loading: false,
        school_id: nextProps.match.params.schoolId,
        rowData: arr
      };
    }
    return null;
  }

  getGridData() {
    return (
      <div className="ag-theme-material ag-custom-styles device-grid">
        <div />
        {this.state.loading ? (
          <div className="loader">
            <Spinner size="large" />
          </div>
        ) : (
          <AgGridReact
            enableSorting={true}
            rowHeight="48"
            headerHeight="36"
            columnDefs={this.state.columnDefs}
            rowData={this.state.rowData}
            onGridReady={this.onGridReady.bind(this)}
            onRowClicked={this.onRowClicked}
          />
        )}
      </div>
    );
  }

  setRowRedirect = (schoolId) => {
    this.setState({
      selectedSchool: schoolId,
      redirect: true
    });
  };

  onRowClicked = (event) => {
    let schoolId = event.data._id;
    this.setRowRedirect(schoolId);
  };

  renderRowRedirect = () => {
    if (this.state.redirect) {
      let schoolId = this.state.selectedSchool;
      return (
        <Redirect
          to={{
            pathname: '/app/facilityItem/' + schoolId,
            state: { data: this.state.rowData }
          }}
        />
      );
    }
  };

  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    params.api.sizeColumnsToFit();
    window.addEventListener('resize', function() {
      setTimeout(function() {
        params.api.sizeColumnsToFit();
      });
    });
    params.api.sizeColumnsToFit();
  }

  onClickBack = () => {
    this.props.history.push('/app/schools');
  };

  SchooladdFacility = () => {
    this.props.history.push({
      pathname: '/app/addFacility',
      state: { schoolId: this.props.match.params.SchoolId }
    });
  };

  EditSchool = () => {
    this.setState({
      isEdit: !this.state.isEdit
    });
  };

  getName = (value) => {
    if (value === 'name') {
      if (this.state.rowData && this.state.rowData.length) {
        return this.state.rowData
          ? this.state.rowData[0].school_data[0].name
          : '';
      } else {
        return '';
      }
    }
    if (value === 'city') {
      if (this.state.rowData && this.state.rowData.length) {
        return this.state.rowData
          ? this.state.rowData[0].school_data[0].city
          : '';
      } else {
        return '';
      }
    }
    if (value === 'state') {
      if (this.state.rowData && this.state.rowData.length) {
        return this.state.rowData
          ? this.state.rowData[0].school_data[0].state
          : '';
      } else {
        return '';
      }
    }
  };

  getSchoolData = () => {
    if (this.state.rowData && this.state.rowData.length) {
      return this.state.rowData ? this.state.rowData : null;
    } else {
      return null;
    }
  };

  switchTabs = (value) => {
    if (value === 'facilities') {
      this.setState({
        tab: value
      });
    } else if (value === 'teams') {
      this.setState({
        tab: value
      });
    } else if (value === 'schedules') {
      console.log(value);
    } else if (value === 'users') {
      this.setState({
        tab: value
      });
    }
  };

  render() {
    return (
      <div className="full-width-container">
        {this.state.isEdit ? (
          <AddSchoolForm
            type="edit"
            cancel={this.EditSchool}
            schoolData={this.getSchoolData()}
          />
        ) : (
          <div>
            <div className="left-container">
              <SchoolStadiumLeftNav
                switchTabs={this.switchTabs}
                onClickBack={this.onClickBack}
                pathname={this.props.match.url}
                currentTab={this.state.tab}
                name={this.getName('name')}
                city={this.getName('city')}
                state={this.getName('state')}
                editSchool={this.EditSchool}
                school_data={this.getSchoolData()}
              />
            </div>
            <div className="right-container">
              {this.state.tab === 'facilities' ? (
                <div>
                  <Header
                    schooladdFacility={this.SchooladdFacility}
                    schoolName={this.getName('name')}
                  />
                  {this.getGridData()}
                  {this.renderRowRedirect()}
                </div>
              ) : null}
              <div>
                {this.state.tab === 'teams' ? (
                  <Route
                    path={`${this.props.match.url}/:` + this.state.tab}
                    component={teams}
                  />
                ) : null}
              </div>
              <div>
                {this.state.tab === 'users' ? (
                  <Route
                    path={`${this.props.match.url}/:` + this.state.tab}
                    component={users}
                  />
                ) : null}
              </div>
            </div>
          </div>
        )}
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  facilityList: state.appData.facilityList
});

export default connect(
  mapStateToProps,
  { getFacilityList }
)(FacilityList);
