const express = require('express');
const generateControllers = require('../../util/query');
const Streeming_device = require('./model');

const StreemingDeviceController = generateControllers(Streeming_device);
const streemingDeviceRouter = express.Router();

streemingDeviceRouter
  .route('/')
  .get(StreemingDeviceController.getAll)
  .post(StreemingDeviceController.createOne);

streemingDeviceRouter
  .route('/:id')
  .get(StreemingDeviceController.getOne)
  .put(StreemingDeviceController.updateOne)
  .delete(StreemingDeviceController.deleteOne);

module.exports = streemingDeviceRouter;
