const mongoose = require('mongoose');

const GameSchema = new mongoose.Schema(
  {
    created_ts: { type: String },
    device_serial: { type: String },
    finalized: { type: Boolean },
    finalized_confirmed: { type: Boolean },
    game_id: { type: String },
    last_update_ts: { type: Date },
    name: { type: String },
    schedule_id: { type: mongoose.Schema.Types.ObjectId, ref: 'Schedule' },
    scheduled_date: { type: Date },
    sport: { type: String },
    state: mongoose.Schema.Types.Mixed,
    team_away_id: { type: mongoose.Schema.Types.ObjectId, ref: 'Team' },
    team_home_id: { type: mongoose.Schema.Types.ObjectId, ref: 'Team' }
  },
  { versionKey: false }
);

const Game = mongoose.model('Game', GameSchema);

module.exports = Game;
