import { SHOW_CONTAINER_MENU, HIDE_CONTAINER_MENU } from '../actions/types';

const initialState = {
  displayContainerNav: false
};

const layout = (state = initialState, action) => {
  switch (action.type) {
    case SHOW_CONTAINER_MENU:
      return { displayContainerNav: true };
    case HIDE_CONTAINER_MENU:
      return { displayContainerNav: false };
    default:
      return state;
  }
};

export default layout;
