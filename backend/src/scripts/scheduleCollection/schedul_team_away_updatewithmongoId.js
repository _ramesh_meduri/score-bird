const mongoose = require('mongoose');
const Schedule = require('../../resources/schedule/model');
const data = require('./schedule_team_away_id_map.json');

function main() {
  mongoose.Promise = global.Promise;
  mongoose
    .connect('mongodb://localhost:27017/scorebird_customer', {
      useNewUrlParser: true
    })
    .then(() => {
      console.log('MongoDB Connected !');
      start();
    })
    .catch((err) => console.log(err));
}

const start = async () => {
  let len = data.length;
  for (let i = 0; i < len; i++) {
    let obj = data[i];

    let scheduleObj = await Schedule.findOne({ _id: obj.schedule_id });
    scheduleObj.team_away_id = obj.team_away_id;
    delete scheduleObj._id;

    let updatedObj = await Schedule.findByIdAndUpdate(
      obj.schedule_id,
      scheduleObj,
      { new: true }
    );
  }
  console.log('completed!');
};

main();
