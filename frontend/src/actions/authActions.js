import axios from 'axios';
import { LOGIN_USER, LOGOUT_USER, INIT_URL } from './types';
import setAuthToken from '../util/setAuthToken';

const loginUser = (userData) => (dispatch) => {
  axios
    .post('/auth/login', userData)
    .then((res) => {
      const userObj = res.data;
      localStorage.setItem('user', JSON.stringify(userObj));
      setAuthToken(userObj.token);
      dispatch({ type: LOGIN_USER, payload: userObj });
    })
    .catch((err) => {
      console.log(err);
    });
};

const logoutUser = () => (dispatch) => {
  localStorage.removeItem('user');
  setAuthToken(false);
  dispatch({ type: LOGOUT_USER });
};

const setInitUrl = (url) => (dispatch) => {
  dispatch({ type: INIT_URL, payload: url });
};

export { loginUser, logoutUser, setInitUrl };
