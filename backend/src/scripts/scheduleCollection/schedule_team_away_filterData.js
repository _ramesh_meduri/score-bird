//schedules.team_away_id=teams._id

const path = require('path');
const fs = require('fs');
const mongoose = require('mongoose');
const Schedule = require('../../resources/schedule/model');
const Team = require('../../resources/team/model');

function main() {
  mongoose.Promise = global.Promise;
  mongoose
    .connect('mongodb://localhost:27017/scorebird_customer', {
      useNewUrlParser: true
    })
    .then(() => {
      console.log('MongoDB Connected !');
      start();
    })
    .catch((err) => console.log(err));
}

function start() {
  Schedule.find()
    .then(cb)
    .catch((err) => {
      console.log(err);
    });
}

let cb = async (docs) => {
  let toBeProcessedArr = [];
  let scheduleArr = docs.map((d) => d.toObject());
  let len = scheduleArr.length;
  scheduleArr.forEach((item) => {
    if (item.team_away_id) {
      toBeProcessedArr.push({
        schedule_id: item._id,
        team_away_id: item.team_away_id
      });
    }
  });
  let i, obj, teamDoc;
  len = toBeProcessedArr.length;
  for (i = 0; i < len; i++) {
    obj = toBeProcessedArr[i];
    teamDoc = await Team.findOne({ team_id: obj.team_away_id });
    if (teamDoc) {
      obj.team_away_id = teamDoc._id;
    }
  }
  let filePath = path.join(__dirname, 'schedule_team_away_id_map.json');
  console.log(filePath);
  fs.writeFile(filePath, JSON.stringify(toBeProcessedArr, null, 2), (err) => {
    if (err) {
      return console.log(err);
    }
    console.log('The file was saved!');
  });
};

main();
