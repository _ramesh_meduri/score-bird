//school.org_id ---- org._id

const mongoose = require('mongoose');
const School = require('../../resources/school/model');
const data = require('./org_id_org_map.json');

function main() {
  mongoose.Promise = global.Promise;
  mongoose
    .connect('mongodb://localhost:27017/scorebird_customer', {
      useNewUrlParser: true
    })
    .then(() => {
      console.log('MongoDB Connected !');
      start();
    })
    .catch((err) => console.log(err));
}

const start = async () => {
  let len = data.length;
  for (let i = 0; i < len; i++) {
    let obj = data[i];

    let schoolObj = await School.findOne({ _id: obj.school_id });
    schoolObj.org_id = obj.org_id;
    delete schoolObj._id;

    let updatedSchoolObj = await School.findByIdAndUpdate(
      obj.school_id,
      schoolObj,
      {
        new: true
      }
    );
  }
  console.log('Completed !');
};

main();
