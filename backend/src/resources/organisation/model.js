const mongoose = require('mongoose');

const OrganisationSchema = new mongoose.Schema(
  {
    name: { type: String },
    created_by: { type: mongoose.Schema.Types.ObjectId, ref: 'User' }
  },
  { versionKey: false }
);

const Organisation = mongoose.model('Organisation', OrganisationSchema);

module.exports = Organisation;
