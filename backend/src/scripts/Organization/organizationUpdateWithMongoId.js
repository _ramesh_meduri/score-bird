const mongoose = require('mongoose');
const Organisation = require('../../resources/organisation/model');
const data = require('./created_by_user_map.json');

function main() {
  mongoose.Promise = global.Promise;
  mongoose
    .connect('mongodb://localhost:27017/scorebird_customer', {
      useNewUrlParser: true
    })
    .then(() => {
      console.log('MongoDB Connected !');
      start();
    })
    .catch((err) => console.log(err));
}

const start = async () => {
  let len = data.length;
  for (let i = 0; i < len; i++) {
    let obj = data[i];

    let orgObj = await Organisation.findOne({ _id: obj.org_id });
    orgObj.created_by = obj.user_id;
    delete orgObj._id;

    let updatedOrgObj = await Organisation.findByIdAndUpdate(
      obj.org_id,
      orgObj,
      {
        new: true
      }
    );
  }
  console.log('Completed !');
};

main();
