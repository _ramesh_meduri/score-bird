const express = require('express');
const generateControllers = require('../../util/query');
const FacilityType = require('./model');

const facilityTypeController = generateControllers(FacilityType);
const facilityTypeRouter = express.Router();

facilityTypeRouter
  .route('/')
  .get(facilityTypeController.getAll)
  .post(facilityTypeController.createOne);

facilityTypeRouter
  .route('/:id')
  .get(facilityTypeController.getOne)
  .put(facilityTypeController.updateOne)
  .delete(facilityTypeController.deleteOne);

module.exports = facilityTypeRouter;
