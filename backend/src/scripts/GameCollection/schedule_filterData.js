//games.schedule_id=schedules._id

const path = require('path');
const fs = require('fs');
const mongoose = require('mongoose');
const Game = require('../../resources/game/model');
const Schedule = require('../../resources/schedule/model');

function main() {
  mongoose.Promise = global.Promise;
  mongoose
    .connect('mongodb://localhost:27017/scorebird_customer', {
      useNewUrlParser: true
    })
    .then(() => {
      console.log('MongoDB Connected !');
      start();
    })
    .catch((err) => console.log(err));
}

function start() {
  Game.find()
    .then(cb)
    .catch((err) => {
      console.log(err);
    });
}

let cb = async (docs) => {
  let toBeProcessedArr = [];
  let gameArr = docs.map((d) => d.toObject());
  let len = gameArr.length;
  gameArr.forEach((item) => {
    if (item.schedule_id) {
      toBeProcessedArr.push({
        game_id: item._id,
        schedule_id: item.schedule_id
      });
    }
  });
  let i, obj, scheduleDoc;
  len = toBeProcessedArr.length;
  for (i = 0; i < len; i++) {
    obj = toBeProcessedArr[i];
    //console.log(obj);
    scheduleDoc = await Schedule.findOne({ schedule_id: obj.schedule_id });
    obj.schedule_id = scheduleDoc._id;
  }
  let filePath = path.join(__dirname, 'game_schedule_map.json');
  console.log(filePath);
  fs.writeFile(filePath, JSON.stringify(toBeProcessedArr, null, 2), (err) => {
    if (err) {
      return console.log(err);
    }
    console.log('The file was saved!');
  });
};

main();
