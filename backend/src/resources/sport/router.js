const express = require('express');
const generateControllers = require('../../util/query');
const Sport = require('./model');

const sportController = generateControllers(Sport);
const sportRouter = express.Router();

sportRouter
  .route('/')
  .get(sportController.getAll)
  .post(sportController.createOne);

sportRouter
  .route('/:id')
  .get(sportController.getOne)
  .put(sportController.updateOne)
  .delete(sportController.deleteOne);

module.exports = sportRouter;
