const mongoose = require('mongoose');

const VenueSchema = new mongoose.Schema(
  {
    date_added: Date,
    name: String
  },
  { versionKey: false }
);

const Venue = mongoose.model('Venue', VenueSchema);

module.exports = Venue;
