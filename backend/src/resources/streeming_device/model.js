const mongoose = require('mongoose');

const StreemingDeviceSchema = new mongoose.Schema(
  {
    deviceList: { type: Array }
  },
  { versionKey: false }
);

const StreemingDevice = mongoose.model(
  'StreemingDevice',
  StreemingDeviceSchema
);

module.exports = StreemingDevice;
