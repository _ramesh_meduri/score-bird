const mongoose = require('mongoose');

const TeamSchema = new mongoose.Schema(
  {
    gender: { type: String },
    integration_id: { type: String },
    last_update_ts: { type: Date },
    meta: mongoose.Schema.Types.Mixed,
    name: { type: String },
    school: { type: String },
    sport: { type: String },
    state: { type: String },
    team_grade: { type: String },
    type: { type: String },
    school_id: { type: mongoose.Schema.Types.ObjectId, ref: 'School' }
  },
  { versionKey: false }
);

const Team = mongoose.model('Team', TeamSchema);

module.exports = Team;
