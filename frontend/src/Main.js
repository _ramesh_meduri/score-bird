import React from 'react';
import { ConnectedRouter } from 'connected-react-router';
import { Route } from 'react-router-dom';
import setAuthToken from './util/setAuthToken';
import App from './containers/App';

let userStr = localStorage.getItem('user');

if (userStr) {
  let user = JSON.parse(userStr);
  if (user.token) {
    setAuthToken(user.token);
  }
}

const Main = ({ history }) => {
  return (
    <ConnectedRouter history={history}>
      <Route path="/" component={App} />
    </ConnectedRouter>
  );
};

export default Main;
