//school.org_id ---- org._id

const path = require('path');
const fs = require('fs');
const mongoose = require('mongoose');

const School = require('../../resources/school/model');
const Organisation = require('../../resources/organisation/model');

function main() {
  mongoose.Promise = global.Promise;
  mongoose
    .connect('mongodb://localhost:27017/scorebird_customer', {
      useNewUrlParser: true
    })
    .then(() => {
      console.log('MongoDB Connected !');
      start();
    })
    .catch((err) => console.log(err));
}

function start() {
  School.find()
    .then(cb)
    .catch((err) => {
      console.log(err);
    });
}

let cb = async (docs) => {
  let toBeProcessedArr = [];
  let schoolArr = docs.map((d) => d.toObject());
  let len = schoolArr.length;
  schoolArr.forEach((schoolObj) => {
    if (schoolObj.org_id) {
      toBeProcessedArr.push({
        school_id: schoolObj._id,
        org_id: schoolObj.org_id
      });
    }
  });
  let i = 0,
    obj,
    orgDoc;
  len = toBeProcessedArr.length;
  for (i; i < len; i++) {
    obj = toBeProcessedArr[i];
    orgDoc = await Organisation.findOne({ unique_id: obj.org_id });
    if (orgDoc) {
      obj.org_id = orgDoc._id;
    }
  }
  let filePath = path.join(__dirname, 'org_id_org_map.json');
  console.log(filePath);
  fs.writeFile(filePath, JSON.stringify(toBeProcessedArr, null, 2), (err) => {
    if (err) {
      return console.log(err);
    }
    console.log('The file was saved!');
  });
};

main();
