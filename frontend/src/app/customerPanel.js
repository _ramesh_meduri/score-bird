import React, { Component } from 'react';
import { Switch, Route, NavLink } from 'react-router-dom';
import { connect } from 'react-redux';
import { logoutUser } from '../actions/authActions';
import { showContainerMenu, hideContainerMenu } from '../actions/layoutActions';
import {
  ContainerHeader,
  GroupHeading,
  HeaderSection,
  Item,
  ItemAvatar,
  LayoutManager,
  MenuSection,
  GlobalNav,
  NavigationProvider,
  Separator,
  modeGenerator,
  ThemeProvider,
  GlobalItem
} from '@atlaskit/navigation-next';
import SchoolList from './School/schoolList';
import FacilityList from './School/facilityList';
import FacilityItem from './School/facilityItem';
import AddFacility from './School/addFacility';

import logo from '../assets/images/sb_logo_menu.svg';
import logoIcon from '../assets/images/menu_logo.svg';
import schoolIcon from '../assets/images/menu-icons/schools.svg';
// import facilitiesIicon from '../../assets/images/menu-icons/facilities.svg';
// import teamsIicon from '../../assets/images/menu-icons/teams.svg';
// import gamesIicon from '../../assets/images/menu-icons/games.svg';
// import usersIicon from '../../assets/images/menu-icons/users.svg';

import Avatar from '@atlaskit/avatar';
import AddIcon from '@atlaskit/icon/glyph/add';
import BacklogIcon from '@atlaskit/icon/glyph/backlog';
import BoardIcon from '@atlaskit/icon/glyph/board';
import GraphLineIcon from '@atlaskit/icon/glyph/graph-line';
import ShortcutIcon from '@atlaskit/icon/glyph/shortcut';
import MenuIcon from '@atlaskit/icon/glyph/menu';
import InlineDialog from '@atlaskit/inline-dialog';
import OfficeBuildingFilledIcon from '@atlaskit/icon/glyph/office-building-filled';
import AppAccessIcon from '@atlaskit/icon/glyph/app-access';
import SignOutIcon from '@atlaskit/icon/glyph/sign-out';
import {
  DropdownItemGroup,
  DropdownItem,
  DropdownMenuStateless
} from '@atlaskit/dropdown-menu';

import './customerStyle.css';

class GlobalItemWithDropdown extends Component {
  state = {
    isOpen: false
  };
  handleOpenChange = ({ isOpen }) => this.setState({ isOpen });
  render() {
    const { items, trigger: Trigger } = this.props;
    const { isOpen } = this.state;
    return (
      <DropdownMenuStateless
        boundariesElement="window"
        isOpen={isOpen}
        onOpenChange={this.handleOpenChange}
        position="right bottom"
        trigger={<Trigger isOpen={isOpen} />}
      >
        {items}
      </DropdownMenuStateless>
    );
  }
}

const ItemComponent = ({ dropdownItems: DropdownItems, ...itemProps }) => {
  if (DropdownItems) {
    return (
      <GlobalItemWithDropdown
        trigger={({ isOpen }) => (
          <GlobalItem isSelected={isOpen} {...itemProps} />
        )}
        items={<DropdownItems />}
      />
    );
  }
  return <GlobalItem {...itemProps} />;
};

const globalNavPrimaryItems = (props) => {
  return [
    {
      id: 'logo',
      icon: () => (
        <div className="global_nav_logo">
          <img src={logoIcon} alt="logo" />
        </div>
      ),
      label: 'Customer Portal'
    },
    {
      id: 'menu',
      icon: MenuIcon,
      label: 'Menu',
      onClick: () => {
        props.onMenuClick();
      }
    },
    // { id: "search", icon: SearchIcon, label: "Search" },
    { id: 'create', icon: AddIcon, label: 'Add' }
  ];
};

const ProfileDropdown = ({ logoutUser }) => {
  return (
    <DropdownItemGroup>
      <DropdownItem elemBefore={<OfficeBuildingFilledIcon />}>
        Organisation
      </DropdownItem>
      <DropdownItem elemBefore={<AppAccessIcon />}>Account</DropdownItem>
      <DropdownItem elemBefore={<SignOutIcon />} onClick={() => logoutUser()}>
        Sign Out
      </DropdownItem>
    </DropdownItemGroup>
  );
};

const connectedProfileDropdown = connect(
  null,
  { logoutUser }
)(ProfileDropdown);

const globalNavSecondaryItems = (props) => {
  return [
    {
      id: '10-composed-navigation-2',
      component: ({ className, onClick }) => (
        <span className={className}>
          <Avatar
            borderColor="transparent"
            isActive={false}
            isHover={false}
            size="small"
            onClick={onClick}
          />
        </span>
      ),
      label: 'Profile',
      size: 'small',
      dropdownItems: connectedProfileDropdown
    }
  ];
};

const GlobalNavigation = (props) => {
  return (
    <div data-webdriver-test-key="global-navigation" className="global-menubar">
      <GlobalNav
        itemComponent={ItemComponent}
        primaryItems={globalNavPrimaryItems(props)}
        secondaryItems={globalNavSecondaryItems(props)}
      />
    </div>
  );
};

const ProductNavigation = (props) => (
  <div className="contextual-menu" style={{ display: 'none' }}>
    <div
      data-webdriver-test-key="product-navigation"
      className="product-navigation"
    >
      <HeaderSection>
        {({ className }) => (
          <div className="logo-container">
            <NavLink to="/">
              <img src={logo} className="scorebird-logo" alt="logo" />
            </NavLink>
          </div>
        )}
      </HeaderSection>
      <MenuSection className="menu-links">
        {({ className }) => (
          <div className={className}>
            <div className={className}>
              <div className="menu-items">
                <NavLink to="/" exact={true} onClick={props.onNavClick}>
                  <Item
                    text="Schools"
                    isActive={false}
                    before={() => (
                      <img className="custom-icon" src={schoolIcon} />
                    )}
                  />
                </NavLink>
              </div>
            </div>
          </div>
        )}
      </MenuSection>
    </div>
  </div>
);

const customThemeMode = modeGenerator({
  product: {
    text: '#FFF',
    background: '#3240a0'
  }
});

class CustomerPanel extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dialogOpen: false
    };
  }

  ContainerNavigation = () => (
    <div data-webdriver-test-key="container-navigation">
      <HeaderSection>
        {({ css }) => (
          <div data-webdriver-test-key="container-header">
            <ContainerHeader
              before={(itemState) => (
                <ItemAvatar
                  itemState={itemState}
                  appearance="square"
                  size="large"
                />
              )}
              text="My software project"
              subText="Software project"
            />
          </div>
        )}
      </HeaderSection>
      <MenuSection>
        {({ className }) => (
          <div className={className}>
            <Item
              before={BacklogIcon}
              text="Backlog"
              isSelected
              testKey="container-item-backlog"
            />
            <Item
              before={BoardIcon}
              text="Active sprints"
              testKey="container-item-sprints"
            />
            <Item
              before={GraphLineIcon}
              text="Reports"
              testKey="container-item-reports"
            />
            <Separator />
            <GroupHeading>Shortcuts</GroupHeading>
            <Item before={ShortcutIcon} text="Project space" />
            <Item before={ShortcutIcon} text="Project repo" />
            <InlineDialog
              onClose={() => {
                this.setState({ dialogOpen: false });
              }}
              content={<div>Renders correctly without getting chopped off</div>}
              isOpen={this.state.dialogOpen}
              placement="right"
            >
              <Item
                onClick={() => {
                  this.setState({ dialogOpen: true });
                }}
                before={GraphLineIcon}
                text="Item with InlineDialog"
                testKey="container-item-click"
              />
            </InlineDialog>
          </div>
        )}
      </MenuSection>
    </div>
  );

  handleMenuClick = () => {
    this.props.showContainerMenu();
  };

  handleNavClick = () => {
    this.props.hideContainerMenu();
  };

  render() {
    const { match } = this.props;
    console.log('------customerpanel--------');
    console.log(this.props);
    console.log('-------customerpanel-------');
    return (
      <div className="App admin-panel body-container">
        <NavigationProvider initialUIController={{ isResizeDisabled: true }}>
          <ThemeProvider
            theme={(theme) => ({ ...theme, mode: customThemeMode })}
          >
            <LayoutManager
              globalNavigation={() => (
                <GlobalNavigation onMenuClick={this.handleMenuClick} x="10" />
              )}
              productNavigation={() => (
                <ProductNavigation onNavClick={this.handleNavClick} />
              )}
              containerNavigation={null}
              collapseToggleTooltipContent={false}
            >
              <div data-webdriver-test-key="content">
                <div className="Routes-paths">
                  <Switch>
                    <Route
                      exact
                      path={`${match.url}/schools`}
                      component={SchoolList}
                    />
                    <Route
                      exact
                      path={`${match.url}/facilities/:schoolId`}
                      component={FacilityList}
                    />
                    <Route
                      exact
                      path={`${match.url}/facilityItem/:facilityId`}
                      component={FacilityItem}
                    />
                    <Route
                      exact
                      path={`${match.url}/addFacility`}
                      component={AddFacility}
                    />
                  </Switch>
                </div>
              </div>
            </LayoutManager>
          </ThemeProvider>
        </NavigationProvider>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  user: state.auth.user.email,
  displayContainerNav: state.layout.displayContainerNav
});

export default connect(
  mapStateToProps,
  { showContainerMenu, hideContainerMenu }
)(CustomerPanel);
