import React, { Fragment, Component } from 'react';
import { connect } from 'react-redux';
import Select from '@atlaskit/select';
import TextField from '@atlaskit/textfield';

import { Checkbox } from '@atlaskit/checkbox';
import { FieldTextAreaStateless } from '@atlaskit/field-text-area';
import Button from '@atlaskit/button';
import Form, { Field, CheckboxField, Fieldset } from '@atlaskit/form';
import ArrowLeftIcon from '@atlaskit/icon/glyph/arrow-left';
import {
  getFacilityTypes,
  getScoreboardTypes,
  postFacility
} from '../../actions/schoolActions';
import stateArr from '../../assets/json/states.json';
import cityObj from '../../assets/json/cities.json';



class SchooladdFacility extends Component {
  constructor(props) {
    super(props);

    let cityArr = [];

    this.state = {
      isFacility: [
        { label: 'No', value: 'no' },
        { label: 'Yes', value: 'yes' }
      ],
      loading: true,
      schools: [],
      scoreboardType: '',
      facilityType: '',
      is_facility:'',
      stateArr: stateArr,
      selectedState: '',
      cityArr: cityArr,
      selectedCity: ''
    };
  }

  changeState = (e, value) => {
    if (value === 'isfacility') {
      this.setState({ is_facility: e.value });
    }
    if (value === 'city') {
      this.setState({
        selectedCity: e
      });
    }

    if (value === 'scoreboardtype') {
      this.setState({ scoreboardType: { label: e.label, value: e.value } });
    }

    if (value === 'facilitytype') {
      this.setState({ facilityType: { label: e.label, value: e.value } });
    }
    if (value === 'state') {
      console.log(e);
      let v = e.value;
      console.log(v);

      let cityItems = cityObj[v];
      let cityArr = cityItems.map((item) => ({
        label: item,
        value: item
      }));

      this.setState({
        selectedState: v,
        cityArr: cityArr
      });
      setTimeout(() =>{
         console.log(this.state);
     },1000)
      
    }
   
  };

  componentDidMount() {
    this.props.getFacilityTypes();
    this.props.getScoreboardTypes();
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.facilityTypes && nextProps.scoreboardTypes) {
      

      let facilityTypesArr = nextProps.facilityTypes.map((item) => ({
        label: item.name,
        value: item._id
      }));


      console.log('--------------facilityTypesArr--------------');
      console.log(JSON.stringify(facilityTypesArr));
      console.log('--------------facilityTypesArr--------------');

      let scoreboardTypesArr = nextProps.scoreboardTypes.map((item) => ({
        label: item.name,
        value: item._id
      }));

      console.log('--------------scoreboardTypesArr--------------');
      console.log(JSON.stringify(scoreboardTypesArr));
      console.log('--------------scoreboardTypesArr--------------');

      return {
        ...prevState,
        loading: false,
        facilityTypes: facilityTypesArr,
        scoreboardTypes: scoreboardTypesArr
      };
    }
    return null;
  }

  goToFacilitiesList = () => {
    console.log(this.props);
    this.props.history.push(
      '/app/facilities/' + this.props.selectedSchool._id
    );
  };

  render() {
    let { facilityTypes, scoreboardTypes, isFacility } = this.state;
    facilityTypes = facilityTypes || [];
    scoreboardTypes = scoreboardTypes || [];
    return (
      <div className="full-width-container">
        {this.props.type !== 'edit' ? (
          <div className="content-header">
            <div className="content-header-inner header-row">
              <div className="left-panel back-with-heading">
                <div className="back-to-text" onClick={this.goToFacilitiesList}>
                  <span className="back-icon" to="/">
                    <ArrowLeftIcon />
                  </span>
                  <b>Back To All Facilities </b>
                </div>
              </div>
              <div className="right-panel" />
            </div>
          </div>
        ) : null}
        {!this.state.loading ? (
          <div className="add-form">
            <Form
              onSubmit={(data) => {
                if (this.props.type === 'edit') {
                  let updatefacilityData = [
                    {
                      ethernet_access: data.internet[0] ? 'Yes' : 'No',
                      name: data.name,
                      school_id: data.school_id,
                      state: this.state.selectedState ,
                      city: data.city ? data.city.value : '',
                      sport: data.sport,
                      wifi_access: data.internet[1] ? 'Yes' : 'No',
                      scoreboard_id: this.state.scoreboardType.value,
                      is_facility: this.state.is_facility,
                      facility_type_id: this.state.facilityType.value,
                      facilityId: this.props.facilityId
                    }
                  ];
                  this.sbs.saveFacilityDetails(updatefacilityData).then(() => {
                    this.props.cancel();
                  });
                } else {
                  data.school_id = this.props.location.state.schoolId;
                  let facilityObj = {
                    ethernet_access: data.internet[0] ? 'Yes' : 'No',
                    name: data.name,
                    school_id: data.school_id,
                    state: this.state.selectedState ,
                    city: data.city ? data.city.value : '',
                    sport: data.sport,
                    wifi_access: data.internet[1]? 'Yes' : 'No',
                    scoreboard_id: this.state.scoreboardType.value,
                    is_facility: this.state.is_facility,
                    facility_type_id: this.state.facilityType.value
                  };

                  console.log('-----------PAYLOAD------------');
                  console.log(JSON.stringify(data));
                  console.log('------------PAYLOAD-----------');
                  this.props.postFacility(facilityObj);
                }
              }}
            >
              {({ formProps }) => (
                <form {...formProps}>
                  <div className="scrollable">
                    <div className="row">
                      <div className="left-section-form">
                        <div className="row">
                          <Field
                            name="facilitytype"
                            label="Facility Type"
                            defaultValue={this.state.facilityType}
                            value={this.state.facilityType}
                            className="facility-spec"
                          >
                            {({ fieldProps: { id, ...rest } }) => {
                              return (
                                <Fragment>
                                  <Select
                                    inputId={id}
                                    {...rest}
                                    options={facilityTypes}
                                    onChange={(e) =>
                                      this.changeState(e, 'facilitytype')
                                    }
                                  />
                                </Fragment>
                              );
                            }}
                          </Field>

                          <Field
                            name="name"
                            defaultValue={
                              this.props.type === 'edit'
                                ? this.props.facilityData.name
                                : ''
                            }
                            label="Facility Name"
                            className="facility-spec"
                          >
                            {({ fieldProps }) => (
                              <TextField
                                placeholder="enter facility name"
                                {...fieldProps}
                              />
                            )}
                          </Field>
                        </div>

                        <div className="row">
                          <Field
                            name="isfacility"
                            defaultValue={
                              this.props.type == 'edit' &&
                              this.props.facilityData.is_facility
                                ? {
                                    label: this.props.facilityData.is_facility,
                                    value: this.props.facilityData.is_facility
                                  }
                                : {
                                    label: this.state.is_facility,
                                    value: this.state.is_facility
                                  }
                            }
                            value={
                              this.props.type == 'edit'
                                ? {
                                    label: this.props.facilityData.is_facility,
                                    value: this.props.facilityData.is_facility
                                  }
                                : []
                            }
                            label="Is Facility at School"
                            className="facility-spec"
                          >
                            {({ fieldProps: { id, ...rest } }) => (
                              <Fragment>
                                <Select
                                  inputId={id}
                                  {...rest}
                                  options={isFacility}
                                  onChange={(e) =>
                                    this.changeState(e, 'isfacility')
                                  }
                                />
                              </Fragment>
                            )}
                          </Field>
                        </div>
                        {this.state.is_facility === 'yes' ? (
                          <div>
                            <div className="row">
                              <Field
                                name="state"
                                label="State"
                                defaultvalue={this.state.selectedState}
                                value={this.state.selectedState}
                              
                              >
                                {({ fieldProps: { id, ...rest } }) => (
                                  <Fragment>
                                    <Select
                                      inputId={id}
                                      placeholder="state"
                                      {...rest}
                                      options={this.state.stateArr}
                                      
                                      onChange={(e) =>
                                        this.changeState(e, 'state')
                                   
                                      }
                                    />
                                  </Fragment>
                                )}
                              </Field>
                              <Field
                                name="city"
                                label="City"
                                defaultValue={this.state. selectedCity}
                                Value={this.state. selectedCity}
                                className="facility-spec"
                              >
                                {({ fieldProps: { id, ...rest } }) => (
                                  <Fragment>
                                    <Select
                                      inputId={id}
                                      placeholder="city"
                                      {...rest}
                                      options={this.state.cityArr}
                                      onChange={(e) =>
                                        this.changeState(e, 'city')
                                      }
                                    />
                                  </Fragment>
                                )}
                              </Field>
                            </div>

                            <div className="row">
                              <Field
                                name="Facilityaddress"
                                defaultValue=""
                                label="Facility Address"
                                className="facility-spec"
                              >
                                {({ fieldProps }) => (
                                  <TextField
                                    placeholder="enter venue address"
                                    {...fieldProps}
                                  />
                                )}
                              </Field>
                            </div>
                          </div>
                        ) : null}
                      </div>
                      <div className="right-section-form">
                        <div className="fieldset-block">
                          <Fieldset
                            legend="Sport Played at the Facility"
                            className="facility-spec"
                          >
                            <CheckboxField name="sport" value="Football">
                              {({ fieldProps }) => (
                                <Checkbox {...fieldProps} label="Football" />
                              )}
                            </CheckboxField>
                            <CheckboxField name="sport" value="Boys Soccer">
                              {({ fieldProps }) => (
                                <Checkbox {...fieldProps} label="Boys Soccer" />
                              )}
                            </CheckboxField>
                            <CheckboxField name="sport" value="Girls Soccer">
                              {({ fieldProps }) => (
                                <Checkbox
                                  {...fieldProps}
                                  label="Girls Soccer"
                                />
                              )}
                            </CheckboxField>
                          </Fieldset>
                        </div>
                        <Field
                          name="scoreboardtype"
                          label="Scoreboard Type"
                          defaultValue={this.state.scoreboardType}
                          value={this.state.scoreboardType}
                          className="facility-spec"
                        >
                          {({ fieldProps: { id, ...rest } }) => (
                            <Fragment>
                              <Select
                                inputId={id}
                                placeholder="select scoreboared type"
                                {...rest}
                                options={scoreboardTypes}
                                onChange={(e) =>
                                  this.changeState(e, 'scoreboardtype')
                                }
                              />
                            </Fragment>
                          )}
                        </Field>
                        <div className="fieldset-block">
                          <Fieldset
                            legend="Internet available via"
                            className="facility-spec"
                          >
                            <CheckboxField name="internet" value="Ethernet">
                              {({ fieldProps }) => (
                                <Checkbox {...fieldProps} label="Ethernet" />
                              )}
                            </CheckboxField>
                            <CheckboxField name="internet" value="wifi">
                              {({ fieldProps }) => (
                                <Checkbox {...fieldProps} label="wifi" />
                              )}
                            </CheckboxField>
                          </Fieldset>
                        </div>
                        <Field
                          name="venuecomments"
                          defaultValue=""
                          label="Additional comments"
                          className="facility-spec"
                        >
                          {({
                            fieldProps: { isRequired, isDisabled, ...others }
                          }) => (
                            <FieldTextAreaStateless
                              isLabelHidden
                              shouldFitContainer
                              placeholder="enter additional info"
                              disabled={isDisabled}
                              required={isRequired}
                              {...others}
                            />
                          )}
                        </Field>
                      </div>
                    </div>
                  </div>
                  <div className="footer-actions">
                    <div />
                    <div className="actions">
                      {this.props.type == 'edit' ? (
                        <Button
                          type="cancel"
                          appearance="default"
                          onClick={this.props.cancel}
                        >
                          cancel{' '}
                        </Button>
                      ) : (
                        ''
                      )}

                      {this.props.type == 'edit' ? (
                        ''
                      ) : (
                        <Button
                          type="cancel"
                          appearance="default"
                          onClick={this.goToFacilitiesList}
                        >
                          cancel{' '}
                        </Button>
                      )}
                      <Button type="submit" appearance="primary">
                        Save
                      </Button>
                     
                    </div>
                  </div>
                </form>
              )}
            </Form>
          </div>
        ) : null}
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  facilityTypes: state.appData.facilityTypes,
  scoreboardTypes: state.appData.scoreboardTypes,
  stadiumDetails: state.appData.stadiumDetails,
  selectedSchool: state.appData.selectedSchool
});

export default connect(
  mapStateToProps,
  { getFacilityTypes, getScoreboardTypes, postFacility }
)(SchooladdFacility);
