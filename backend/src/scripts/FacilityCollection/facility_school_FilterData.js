// facilities.school_id = school._id

const path = require('path');
const fs = require('fs');
const mongoose = require('mongoose');
const Facility = require('../../resources/facility/model');
const School = require('../../resources/school/model');

function main() {
  mongoose.Promise = global.Promise;
  mongoose
    .connect('mongodb://localhost:27017/scorebird_customer', {
      useNewUrlParser: true
    })
    .then(() => {
      console.log('MongoDB Connected !');
      start();
    })
    .catch((err) => console.log(err));
}

function start() {
  Facility.find()
    .then(cb)
    .catch((err) => {
      console.log(err);
    });
}

let cb = async (docs) => {
  let toBeProcessedArr = [];
  let facilityArr = docs.map((d) => d.toObject());
  let len = facilityArr.length;
  facilityArr.forEach((item) => {
    if (item.school_id) {
      toBeProcessedArr.push({
        facility_id: item._id,
        school_id: item.school_id
      });
    }
  });

  let i, obj, schoolDoc;
  len = toBeProcessedArr.length;
  for (i = 0; i < len; i++) {
    obj = toBeProcessedArr[i];
    schoolDoc = await School.findOne({ unique_id: obj.school_id });
    if (schoolDoc) {
      obj.school_id = schoolDoc._id;
    }
  }
  let filePath = path.join(__dirname, 'facility_school_map.json');
  console.log(filePath);
  fs.writeFile(filePath, JSON.stringify(toBeProcessedArr, null, 2), (err) => {
    if (err) {
      return console.log(err);
    }
    console.log('The file was saved!');
  });
};

main();
