import React, { Component, Fragment } from 'react';
import { Link } from 'react-router-dom';
import Select from '@atlaskit/select';
import TextField from '@atlaskit/textfield';
import { Checkbox } from '@atlaskit/checkbox';
import Form, { Field, CheckboxField, ErrorMessage } from '@atlaskit/form';
import Button from '@atlaskit/button';
import ArrowLeftIcon from '@atlaskit/icon/glyph/arrow-left';

import stateArr from '../../assets/json/states.json';
import cityObj from '../../assets/json/cities.json';
let cityArr = [];


class AddSchoolForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      stateArr: stateArr,
      selectedState: '',
      cityArr: cityArr,
      selectedCity: '',

      ischecked: false,
      

      Address: '',
      PostCode: '',
      mailing_State: [],
      mailing_City: [],
      mailing_Address: '',
      mailing_PostCode: ''
    };
  }
  

  validateFields = (value) => {
   let cond = value && value.length;
    setTimeout(() =>{
    if (!cond) {
      return 'empty';
    }
  },1000)
  
    
  };
  
  goBacktoSchool = () => {
    this.props.history.push('/');
  };

  copyAddress = (e) => {
    this.setState({
      ischecked: !this.state.ischecked,
      isdisabled:!this.state.isdisabled,
      mailing_State: this.state.selectedState,
      mailing_City: this.state.selectedCity,
      mailing_Address: this.state.Address,
      mailing_PostCode: this.state.PostCode
    });
    setTimeout(() => {
      if (!this.state.ischecked) {
        this.setState({
          mailing_State: [],
          mailing_City: [],
          mailing_Address: '',
          mailing_PostCode: ''
        });
      }
    }, 300);
  };
  changeFields = (e, fieldName) => {
    

    if (fieldName === 'address') {
      this.setState({
        Address: e.value
      });
      if (this.state.ischecked) {
        this.setState({
          Address: e.value,
          mailing_Address: e.value,
       
        });

      }
    }

    if (fieldName === 'state') {
      let v = e.value;
      let cityItems = cityObj[v];
      let cityArr = cityItems.map((item) => ({
        label: item,
        value: item
      }));
      
      this.setState({
        selectedState: e,
        cityArr: cityArr
      });
   
        if (this.state.ischecked) {
          this.setState({
            mailing_State: e,
            selectedState: e,
          });
        

        } 
    }

    if (fieldName === 'city') {
      this.setState({
        selectedCity: e
      });
      if (this.state.ischecked) {
        this.setState({
          selectedCity: e,
          mailing_City: e
        });
      }
    }

    if (fieldName === 'postal_code') {
      this.setState({
        PostCode: e.value
      });
      if (this.state.ischecked) {
        this.setState({
          PostCode: e.value,
          mailing_PostCode: e.value
        });
      }
    }
  };
  AddSchool = (obj) => {
  
    console.log('---------150----------');
    console.log(obj);
    console.log('----------152---------');
  };
  render() {
    return (
      <div>
        <div className="content-header">
          <div className="content-header-inner header-row">
            <div className="left-panel back-with-heading">
              <div className="back-to-text" onClick={this.props.cancel}>
                <span className="back-icon" to="/">
                  <ArrowLeftIcon />
                </span>
                <b>
                  <Link
                    className="back-to-text anchor_no_line"
                    to='/app/schools'
                  >
                    Back to Schools
                  </Link>
                </b>
              </div>
            </div>
            <div className="right-panel" />
          </div>
        </div>
        <div className="add-form">
          <Form
            onSubmit={(obj) => {
              // if (this.state.ischecked) {
              //   obj.same_as_Physical_Address = true;
              // } else {
              //   obj.same_as_Physical_Address = false;
              // }
              // this.AddSchool(obj);
              console.log(JSON.stringify(obj));
            }}
          >
            {({ formProps }) => (
              <form {...formProps}>
                <div className="scrollable">
                  <div className="org-details">
                    <Field
                      className="spec"
                      name="name"
                      defaultValue=""
                      label="School/Organisation Name"
                      validate={this.validateFields}
                    >
                      {({ fieldProps, error }) => (
                        <>
                          <TextField
                            placeholder="Enter School/Organisation Name"
                            {...fieldProps}
                          />
                          {error === 'empty' && (
                            <ErrorMessage>
                              Please enter school name
                            </ErrorMessage>
                          )}
                        </>
                      )}
                    </Field>
                    <Field
                      name="logo"
                      defaultValue=""
                      label="School/Organisation logo"
                    >
                      {({ fieldProps }) => (
                        <TextField
                          placeholder="Upload School/Organisation logo"
                          {...fieldProps}
                        />
                      )}
                    </Field>
                    <div className="uploader-block" />
                  </div>
                  <div className="address-row">
                    <div className="physical-address">
                      <h3 className="heading-lineheight">Physical Address</h3>
                      <div>
                        <Field
                          name="address"
                          defaultValue={this.state.Address}
                          value={this.state.Address}
                          label="Address"
                          validate={this.validateFields}
                        >
                          {({ fieldProps, error }) => (
                            <>
                              {' '}
                              <TextField
                                placeholder="Enter Address"
                                {...fieldProps}
                                onKeyUp={(e) => {
                                  this.changeFields(e.target, 'address');
                                }}
                              />
                              {error === 'empty' && (
                                <ErrorMessage>
                                  Please enter address
                                </ErrorMessage>
                              )}
                            </>
                          )}
                        </Field>
                        <Field
                          name="state"
                          label="State"
                          value={this.state.selectedState}
                          validate={this.validateFields}
                        >
                          {({ fieldProps: { id, ...rest }, error }) => (
                            <>
                              <Select
                                inputId={id}
                                placeholder="Auto Suggest"
                                {...rest}
                                options={this.state.stateArr}
                                onChange={(e) => {
                                  this.changeFields(e, 'state');
                                }}
                              />
                              {error === 'empty' && (
                                <ErrorMessage>Please select state</ErrorMessage>
                              )}
                            </>
                          )}
                        </Field>
                        <Field
                          name="city"
                          label="City"
                          value={this.state.selectedCity}
                          validate={this.validateFields}
                        >
                          {({ fieldProps: { id, ...rest }, error }) => (
                            <>
                              <Fragment>
                                <Select
                                  inputId={id}
                                  placeholder="Auto Suggest"
                                  {...rest}
                                  options={this.state.cityArr}
                                  onChange={(e) => {
                                    this.changeFields(e, 'city');
                                  }}
                                />
                              </Fragment>
                              {error === 'empty' && (
                                <ErrorMessage>Please select city</ErrorMessage>
                              )}
                            </>
                          )}
                        </Field>
                        <Field
                          name="postal_Code"
                          defaultValue={this.state.PostCode}
                          value={this.state.PostCode}
                          label="Postal Code"
                          validate={this.validateFields}
                        >
                          {({ fieldProps, error }) => (
                            <>
                              {' '}
                              <TextField
                                placeholder="Enter Postal Code"
                                {...fieldProps}
                                onKeyUp={(e) => {
                                  this.changeFields(e.target, 'postal_code');
                                }}
                              />
                              {error === 'empty' && (
                                <ErrorMessage>
                                  Please enter postalcode
                                </ErrorMessage>
                              )}
                            </>
                          )}
                        </Field>
                      </div>
                    </div>
                    <div className="physical-address">
                      <div className="row align-center">
                        <h3>Mailing Address</h3>
                        <CheckboxField
                          name="same_as_Physical_Address"
                          isChecked={this.state.ischecked}
                          isdisabled={this.state.isdisabled}
                        >
                          {({ fieldProps }) => (
                            <Checkbox
                              {...fieldProps}
                              label="Same as Physical Address"
                              isChecked={this.state.ischecked}
                              isdisabled={this.state.isdisabled}
                              onChange={this.copyAddress}
                            />
                          )}
                        </CheckboxField>
                      </div>
                      <div>
                        <Field
                          name="mailing_address"
                          defaultValue={this.state.mailing_Address}
                          value={this.state.mailing_Address}
                          label="Address"
                        >
                          {({ fieldProps }) => (
                            <TextField
                              placeholder="Enter Address"
                              {...fieldProps}
                            />
                          )}
                        </Field>

                        <Field
                          name="mailing_state"
                          label="State"
                          defaultValue={this.state.mailing_State}
                          value={this.state.mailing_State}
                        >
                          {({ fieldProps: { id, ...rest } }) => (
                            <Fragment>
                              <Select
                                inputId={id}
                                placeholder="Auto Suggest"
                                {...rest}
                                options={this.state.stateArr}
                              />
                            </Fragment>
                          )}
                        </Field>
                        <Field
                          name="mailing_city"
                          label="City"
                          defaultValue={this.state.mailing_City}
                          value={this.state.mailing_City}
                        >
                          {({ fieldProps: { id, ...rest } }) => (
                            <Fragment>
                              <Select
                                inputId={id}
                                placeholder="Auto Suggest"
                                {...rest}
                                options={this.state.cityArr}
                              />
                            </Fragment>
                          )}
                        </Field>
                        <Field
                          name="mailing_postal_Code"
                          defaultValue={this.state.mailing_PostCode}
                          value={this.state.mailing_PostCode}
                          label="Postal Code"
                        >
                          {({ fieldProps }) => (
                            <TextField
                              placeholder="Enter Postal Code"
                              {...fieldProps}
                            />
                          )}
                        </Field>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="footer-actions">
                  <div />
                  <div className="actions">
                    <Button
                      appearance="subtle"
                      className="custom-btn"
                      onClick={this.props.cancel}
                    >
                      Cancel
                    </Button>
                    <Button type="submit" appearance="primary">
                      Add Organisation/School
                    </Button>
                  </div>
                </div>
              </form>
            )}
          </Form>
        </div>
      </div>
    );
  }
}

export default AddSchoolForm;
