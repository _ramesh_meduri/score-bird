const mongoose = require('mongoose');
const Game = require('../../resources/game/model');
const data = require('./game_schedule_map.json');

function main() {
  mongoose.Promise = global.Promise;
  mongoose
    .connect('mongodb://localhost:27017/scorebird_customer', {
      useNewUrlParser: true
    })
    .then(() => {
      console.log('MongoDB Connected !');
      start();
    })
    .catch((err) => console.log(err));
}

const start = async () => {
  let len = data.length;
  for (let i = 0; i < len; i++) {
    let obj = data[i];

    let gameObj = await Game.findOne({ _id: obj.game_id });
    gameObj.schedule_id = obj.schedule_id;
    delete gameObj._id;

    let updatedObj = await Game.findByIdAndUpdate(obj.game_id, gameObj, {
      new: true
    });
  }
  console.log('completed!');
};

main();
