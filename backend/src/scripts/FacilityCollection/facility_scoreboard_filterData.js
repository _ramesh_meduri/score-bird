// facilities.scoreboard_id = scoreboards._id

const path = require('path');
const fs = require('fs');
const mongoose = require('mongoose');
//console.log(mongoose.Types.ObjectId.isValid('5c9c9460e33c9438c0bf263f'));

const Facility = require('../../resources/facility/model');
const Scoreboard = require('../../resources/scoreboard/model');

function main() {
  mongoose.Promise = global.Promise;
  mongoose
    .connect('mongodb://localhost:27017/scorebird_customer', {
      useNewUrlParser: true
    })
    .then(() => {
      console.log('MongoDB Connected !');
      start();
    })
    .catch((err) => console.log(err));
}

function start() {
  Facility.find()
    .then(cb)
    .catch((err) => {
      console.log(err);
    });
}

let cb = async (docs) => {
  let toBeProcessedArr = [];
  let facilityArr = docs.map((d) => d.toObject());
  let len = facilityArr.length;
  console.log(len);
  let filteredArr = facilityArr.filter((item) => {
    if (item.scoreboard_id) {
      return item;
    }
  });
  console.log(filteredArr.length);
  filteredArr.forEach((item) => {
    let id = item.scoreboard_id;
    // let re = /\w{8}-\w{4}-\w{4}-\w{4}-\w{12}/;
    if (mongoose.Types.ObjectId.isValid(id)) {
      toBeProcessedArr.push({
        facility_id: item._id,
        scoreboard_id: id
      });
    }
  });
  console.log(toBeProcessedArr.length);
  let i = 0,
    obj,
    scoreboardDoc;
  len = toBeProcessedArr.length;
  for (i; i < len; i++) {
    obj = toBeProcessedArr[i];
    scoreboardDoc = await Scoreboard.findOne({ unique_id: obj.scoreboard_id });
    obj.scoreboard_id = scoreboardDoc._id;
  }
  let filePath = path.join(__dirname, 'facility_scoreboard_map.json');
  console.log(filePath);
  fs.writeFile(filePath, JSON.stringify(toBeProcessedArr, null, 2), (err) => {
    if (err) {
      return console.log(err);
    }
    console.log('The file was saved!');
  });
};

main();
