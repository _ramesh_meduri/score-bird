const express = require('express');
const AmazonCognitoIdentity = require('amazon-cognito-identity-js');
const CognitoUserPool = AmazonCognitoIdentity.CognitoUserPool;
const AWS = require('aws-sdk');
const request = require('request');
const jwkToPem = require('jwk-to-pem');
const jwt = require('jsonwebtoken');
global.fetch = require('node-fetch');
global.navigator = require('navigator');
const { Login } = require('./auth');

const authRouter = express.Router();
const pool_region = 'us-east-1';
const poolData = {
  UserPoolId: 'us-east-1_lcOC1L8tJ',
  ClientId: '7ap8i1dh75p34a4suajicrf4or'
};
const userPool = new AmazonCognitoIdentity.CognitoUserPool(poolData);

authRouter.post('/login', (req, res) => {
  const { email, password } = req.body;

  const authenticationDetails = new AmazonCognitoIdentity.AuthenticationDetails(
    {
      Username: email,
      Password: password
    }
  );

  const userData = {
    Username: email,
    Pool: userPool
  };

  const cognitoUser = new AmazonCognitoIdentity.CognitoUser(userData);

  let errors = {};
  if (!email) {
    errors.email = 'Email field required';
    return res.status(400).json(errors);
  }
  if (!password) {
    errors.password = 'Password field required';
    return res.status(400).json(errors);
  }
  cognitoUser.authenticateUser(authenticationDetails, {
    onSuccess: function(result) {
      console.log('access token + ' + result.getAccessToken().getJwtToken());
      // console.log('id token + ' + result.getIdToken().getJwtToken());
      // console.log('refresh token + ' + result.getRefreshToken().getToken());
      let token = result.getAccessToken().getJwtToken();
      res.json({
        email: email,
        token: token
      });
      req.user = {
        email: email,
        token: token
      };
    },
    onFailure: function(err) {
      console.log(err);
      errors.email = 'Email not found';
      return res.status(404).json(errors);
    }
  });
});

module.exports = authRouter;
