const mongoose = require('mongoose');

const DeviceSchema = new mongoose.Schema(
  {
    cpu_id: { type: String },
    created_ts: { type: Date },
    device_info: mongoose.Schema.Types.Mixed,
    firmware_version: { type: String },
    home_teams: { type: Array },
    last_online_ts: { type: Date },
    last_update_ts: { type: Date },
    model_variant: { type: String },
    notes: { type: String },
    online: { type: Boolean },
    serial: { type: String },
    sim_id: { type: String }
  },
  { versionKey: false }
);

const Device = mongoose.model('Device', DeviceSchema);

module.exports = Device;
