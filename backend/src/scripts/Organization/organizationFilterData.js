//organisation.created_by=users._id

const path = require('path');
const fs = require('fs');
const mongoose = require('mongoose');

const Organisation = require('../../resources/organisation/model');
const User = require('../../resources/user/model');

function main() {
  mongoose.Promise = global.Promise;
  mongoose
    .connect('mongodb://localhost:27017/scorebird_customer', {
      useNewUrlParser: true
    })
    .then(() => {
      console.log('MongoDB Connected !');
      start();
    })
    .catch((err) => console.log(err));
}

function start() {
  Organisation.find()
    .then(cb)
    .catch((err) => {
      console.log(err);
    });
}

let cb = async (docs) => {
  let toBeProcessedArr = [];
  let orgArr = docs.map((d) => d.toObject());
  let len = orgArr.length;
  orgArr.forEach((item) => {
    if (item.created_by) {
      toBeProcessedArr.push({
        org_id: item._id,
        user_id: item.created_by
      });
    }
  });
  let i, obj, userDoc;
  len = toBeProcessedArr.length;
  for (i = 0; i < len; i++) {
    obj = toBeProcessedArr[i];
    userDoc = await User.findOne({ unique_id: obj.user_id });
    if (userDoc) {
      obj.user_id = userDoc._id;
    }
  }
  let filePath = path.join(__dirname, 'created_by_user_map.json');
  console.log(filePath);
  fs.writeFile(filePath, JSON.stringify(toBeProcessedArr, null, 2), (err) => {
    if (err) {
      return console.log(err);
    }
    console.log('The file was saved!');
  });
};

main();
