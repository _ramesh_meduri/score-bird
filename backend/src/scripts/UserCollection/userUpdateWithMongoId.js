const mongoose = require('mongoose');
const User = require('../../resources/user/model');
const data = require('./user_school_map');

function main() {
  mongoose.Promise = global.Promise;
  mongoose
    .connect('mongodb://localhost:27017/scorebird_customer', {
      useNewUrlParser: true
    })
    .then(() => {
      console.log('MongoDB Connected !');
      start();
    })
    .catch((err) => console.log(err));
}

const start = async () => {
  let len = data.length;
  for (let i = 0; i < len; i++) {
    let obj = data[i];

    let userObj = await User.findOne({ _id: obj.user_id });
    userObj.school_id = obj.school_id;
    delete userObj._id;

    let updatedUserObj = await User.findByIdAndUpdate(obj.user_id, userObj, {
      new: true
    });
  }
  console.log('Completed !');
};

main();
