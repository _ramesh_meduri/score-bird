const mongoose = require('mongoose');

const ScheduleSchema = new mongoose.Schema(
  {
    game_start_ts: { type: String },
    grade: { type: String },
    last_update_ts: { type: Date },
    name: { type: String },
    partner_id: { type: String },
    partner_schedule_id: { type: String },
    schedule_id: { type: String },
    sport: { type: String },
    state: { type: String },
    team_away_id: { type: mongoose.Schema.Types.ObjectId, ref: 'Team' },
    team_away_school: { type: String },
    team_home_id: { type: mongoose.Schema.Types.ObjectId, ref: 'Team' },
    team_home_school: { type: String },
    type: { type: String },
    venue_id: { type: String }
  },
  { versionKey: false }
);

const Schedule = mongoose.model('Schedule', ScheduleSchema);

module.exports = Schedule;
