const mongoose = require('mongoose');

const GroupSchema = new mongoose.Schema(
  {
    group_id: { type: String },
    group_name: { type: String }
  },
  { versionKey: false }
);

const Group = mongoose.model('Group', GroupSchema);

module.exports = Group;
