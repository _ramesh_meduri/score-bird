const mongoose = require('mongoose');

const FacilityTypeSchema = new mongoose.Schema(
  {
    date_added: { type: Date },
    name: { type: String }
  },
  { versionKey: false }
);

const FacilityType = mongoose.model('Facility_types', FacilityTypeSchema);

module.exports = FacilityType;
