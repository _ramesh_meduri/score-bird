import React, { Component } from 'react';
import ArrowLeftIcon from '@atlaskit/icon/glyph/arrow-left';
import schoolIcon from '../../assets/images/menu-icons/school.svg';
import facilityActiveIcon from '../../assets/images/menu-icons/facility-active.svg';

import teamsIcon from '../../assets/images/menu-icons/teams-small.svg';
import usersIcon from '../../assets/images/menu-icons/users-small.svg';
import scheduleIcon from '../../assets/images/menu-icons/schedule.svg';

import { Link } from 'react-router-dom';
import EditFilledIcon from '@atlaskit/icon/glyph/edit-filled';

class SchoolStadiumLeftNav extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="left-nav">
        <div className="leftnav-header">
          <div className="back-to-text" onClick={this.props.onClickBack}>
            <span className="back-icon">
              <ArrowLeftIcon />
            </span>
            Schools
          </div>
        </div>
        <div className="school-details">
          <div className="row">
            <img src={schoolIcon} alt="facility logo" />
            <div className="school-address">
              <label>{this.props.name}</label>
              <p>
                {this.props.city},{this.props.state}
              </p>
            </div>
          </div>
        </div>
        <div className="nav-actions recents">
          <div
            className="sidebar-links"
            type="button"
            onClick={this.props.editSchool}
          >
            <EditFilledIcon />
            Edit School
          </div>
          <Link
            className={
              'sidebar-links ' +
              (this.props.currentTab === 'facilities' ? 'active' : '')
            }
            to={`${this.props.pathname}/facilities`}
            onClick={() => this.props.switchTabs('facilities')}
          >
            <img src={facilityActiveIcon} alt="facility logo" />
            <label>FACILITIES</label>
          </Link>
          <Link
            className={
              'sidebar-links ' +
              (this.props.currentTab === 'teams' ? 'active' : '')
            }
            to={`${this.props.pathname}/teams`}
            onClick={() => this.props.switchTabs('teams')}
          >
            <img src={teamsIcon} alt="teams logo" />
            <label>TEAMS </label>
          </Link>
          <Link
            className={
              'sidebar-links ' +
              (this.props.currentTab === 'schedules' ? 'active' : '')
            }
            to={`${this.props.pathname}/schedules`}
            onClick={() => this.props.switchTabs('schedules')}
          >
            <img src={scheduleIcon} alt="schedule logo" />
            <label>SCHEDULES</label>
          </Link>
          <Link
            className={
              'sidebar-links ' +
              (this.props.currentTab === 'users' ? 'active' : '')
            }
            to={`${this.props.pathname}/users`}
            onClick={() => this.props.switchTabs('users')}
          >
            <img src={usersIcon} alt="users logo" />
            <label>USERS</label>
          </Link>
        </div>
      </div>
    );
  }
}

export default SchoolStadiumLeftNav;
