const mongoose = require('mongoose');
const Team = require('../../resources/team/model');
const data = require('./team_school_map.json');

function main() {
  mongoose.Promise = global.Promise;
  mongoose
    .connect('mongodb://localhost:27017/scorebird_customer', {
      useNewUrlParser: true
    })
    .then(() => {
      console.log('MongoDB Connected !');
      start();
    })
    .catch((err) => console.log(err));
}

const start = async () => {
  let len = data.length;
  for (let i = 0; i < len; i++) {
    let obj = data[i];
    let teamObj = await Team.findOne({ _id: obj.team_id });
    teamObj.school_id = obj.school_id;
    delete teamObj._id;
    let updatedTeamObj = await Team.findByIdAndUpdate(obj.team_id, teamObj, {
      new: true
    });
  }
  console.log('Completed !');
};

main();
