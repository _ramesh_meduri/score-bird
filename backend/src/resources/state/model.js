const mongoose = require('mongoose');

const StateSchema = new mongoose.Schema(
  {
    name: { type: String },
    code: { type: String }
  },
  { versionKey: false }
);

const State = mongoose.model('state', StateSchema);

module.exports = State;
