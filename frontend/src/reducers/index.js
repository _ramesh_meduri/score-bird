import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';

import authReducer from './authReducer';
import layoutReducer from './layoutReducer';

import schoolReducer from './schoolReducer';

const rootReducer = (history) =>
  combineReducers({
    router: connectRouter(history),
    auth: authReducer,
    layout: layoutReducer,

    appData: schoolReducer
  });

export default rootReducer;
