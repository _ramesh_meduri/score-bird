const mongoose = require('mongoose');

const SportSchema = new mongoose.Schema(
  {
    name: { type: String }
  },
  { versionKey: false }
);

const Sport = mongoose.model('sport', SportSchema);

module.exports = Sport;
