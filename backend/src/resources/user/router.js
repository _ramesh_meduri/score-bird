const express = require('express');
const generateControllers = require('../../util/query');
const User = require('./model');

const userController = generateControllers(User);
const userRouter = express.Router();

userRouter
  .route('/')
  .get(userController.getAll)
  .post(userController.createOne);

userRouter
  .route('/:id')
  .get(userController.getOne)
  .put(userController.updateOne)
  .delete(userController.deleteOne);

module.exports = userRouter;
